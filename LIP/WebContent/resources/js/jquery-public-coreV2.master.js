function LoadingDialog(strDivId){
	//strDivId of form #<divId>
	var strTarget;
	
	//Constructor - start
	strTarget = strDivId;
	$(strTarget).dialog({
		modal: true,
		autoOpen: false,
		position: {
			my: 'center',
			at: 'center',
			of: 'body'
		},
		width: 'auto',
		height: 'auto',
		resizable: false
	});
	$('.ui-dialog-titlebar').hide();
	//constructor - end
	
	this.open = function(){
		$(strTarget).dialog('open');
	};
	this.close = function(){
		$(strTarget).dialog('close');							
	};
	return this;
}


function AlertDialog(strAlertDivId, strMsgDivId){
	//strAlertDivId & strMsgDivId of form #<divId>
	var strMsg = "";
	var strAlertTarget;
	var strMsgTarget;
	
	//Constructor - start
	strAlertTarget = strAlertDivId;
	strMsgTarget = strMsgDivId;
	$(strAlertTarget).dialog({
		modal: true,
		autoOpen: false,
		position: {
			my: 'center',
			at: 'center',
			of: 'body'
		},
		width: 'auto',
		height: 'auto',
		title: 'Help',
		resizable: false,
		dialogClass: 'esc-alert',
		buttons: [{
			text: 'Ok',
			click: function(){
				strMsg = "";
				$(strAlertTarget).dialog('close');
			}
		}],								
		open: function(){
			$(strMsgTarget).html(strMsg);
		}
	});
	//Constructor - end	
	
	this.open = function(strMessage, strTitle){
		strMsg = strMessage;
		if(strTitle){
			$(strAlertTarget).dialog('option', 'title', strTitle);
		}
		$(strMsgTarget).html(strMsg);				
		$(strAlertTarget).dialog('open');
	};
	this.close = function(){
		strMsg = "";
		$(strAlertTarget).dialog('close');
	};
	this.isOpen = function(){
		return $(strAlertTarget).dialog('isOpen');
	};
	this.setMessage = function(strMsg){
		$(strMsgTarget).html(strMsg);				
	};

	return this;
}


function zeroFill( number, width ){
	width -= number.toString().length;
	if ( width > 0 ){
	    return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
	}
	return number + ""; // always return a string
}


//BA: [19/11/14] Anti-clickjacking - start
if(self === top){
	   $('#antiClickjack').remove();
}
else{
	   top.location = self.location;
}
//BA: [19/11/14] Anti-clickjacking - end

function getCurrentDatetime(){
	var date = new Date();
	var strDateTime = null;
	strYear = date.getFullYear().toString();
	strMonth = parseInt(date.getMonth()) + 1;
	strMonth = strMonth.toString();
	if(strMonth.length < 2){
		strMonth = "0" + strMonth;
	}
	strDay = date.getDate().toString();
	//console.log("strDay = " + strDay);
	if(strDay.length < 2){
		strDay = "0" + strDay;
	}
	strHours = date.getHours().toString();
	if(strHours.length < 2){
		strHours = "0" + strHours;
	}
	strMinutes = date.getMinutes().toString();
	if(strMinutes.length < 2){
		strMinutes = "0" + strMinutes;
	}
	strSecs = date.getSeconds().toString();
	if(strSecs.length < 2){
		strSecs = "0" + strSecs;
	}
	//strDateTo = strYear + "-" + strMonth + "-" + strDay + "T" + strHours + ":" + strMinutes;
	strDateTime = strYear + "-" + strMonth + "-" + strDay + " " + strHours + ":" + strMinutes + ":" + strSecs;
	return(strDateTime);
}

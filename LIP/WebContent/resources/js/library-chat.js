// JavaScript Document

function addHours(chatTime,hours) {    
   chatTime.setTime(chatTime.getTime() + (hours*(60*60*1000))); 
   return chatTime;   
}

function getOffsetTime(chatTime, offset) {
	var offsetTime = 0;
	var hours = 0;
	
	if (offset < 0) {
		offsetTime = offset * -1;
		hours = hours + offsetTime;
		
	} else if (offset > 0) {
		hours = hours - offset;
		
	} 
	
	chatTime = addHours(chatTime, hours);

	return chatTime;
}

function showChatWidget(position){
	var today = new Date();
	var pcTime = today.getTime();

	/* get current day */
	var tDay = today.getDay();
	
	/* get current time in milliseconds from the epoch 1-1-1970 */
	var tTime = Date.now();
	
	today.setTime(tTime);
	
	var tzOffset = today.getTimezoneOffset()/60;
	

	var chatStart = new Date();
	var chatEnd = new Date();
	
	/* Using UTC time to account for time zone offset during BST */
	
	if ((tDay == 6) & (tzOffset <= -7)) {
		chatStart.setDate(today.getDate()-1);
		chatStart.setHours(9 + (tzOffset * -1));
		chatStart.setMinutes(0);
		chatStart.setSeconds(0);
		chatStart.setMilliseconds(0);		
		chatEnd.setHours((tzOffset * -1) - 7);
		chatEnd.setMinutes(0);		
		chatEnd.setSeconds(0);
		chatEnd.setMilliseconds(0);	
		
	} else if ((tDay == 0) & (tzOffset >= 9)) {
		chatStart.setHours(tzOffset-9);
		chatStart.setMinutes(0);
		chatStart.setSeconds(0);
		chatStart.setMilliseconds(0);
		chatEnd.setDate(today.Date() + 1);
		chatEnd.setHours(17 - tzOffset);
		chatEnd.setMinutes(0);		
		chatEnd.setSeconds(0);
		chatEnd.setMilliseconds(0);
		
	} else if ((tDay > 0) & (tDay < 6)) {
		chatStart.setHours(9);
		chatStart.setMinutes(0);
		chatStart.setSeconds(0);
		chatStart.setMilliseconds(0);
		chatEnd.setHours(17);
		chatEnd.setMinutes(0);		
		chatEnd.setSeconds(0);
		chatEnd.setMilliseconds(0);
	}
					
	/* Work out offset for time zone, parse gets time since 1-1-1970 in milliseconds */	
	var cStart = getOffsetTime(chatStart, tzOffset);
	var cEnd = getOffsetTime(chatEnd, tzOffset);
	
	var chatStart = Date.parse(cStart);
	var chatEnd = Date.parse(cEnd);
	var cs = new Date();
    cs.setTime(cStart);
	var ce = new Date();
	ce.setTime(cEnd);
	
	var showLibraryChat = null;

	if ((tzOffset > -7) & (tDay == 6) & (today.getHours() <= 5)){
		/* if saturday and tz end time after midnight - 5pm + 7 hours */
		if (tTime < chatEnd) {
			showLibraryChat = true;
		} else {
			showLibraryChat = false;
		}
	} else if ((tzOffset > 9) & (tDay == 0) & (today.getHours >= 21)){
		/* if Sunday and tz start time before midnight - 9am - 9 hours */
		if (tTime >= ChatStart) {
			showLibraryChat = true;
		} else {
			showLibraryChat = false;
		}
	} else {

		if ((tTime >= chatStart) & (tTime < chatEnd) & (tDay > 0) & (tDay < 6)) {
			showLibraryChat = true;
		} else {
			showLibraryChat = false;
		}
	}

	if (position == "left") {	
		if (showLibraryChat) {
		//<!-- Library Chat -->
			document.getElementById("live-chat").innerHTML = "<div class=\"chat-lh-placeholder\"><div class=\"needs-js\">JavaScript disabled or chat unavailable. "+today+" "+tDay+"</div></div><div class=\"chat-service-lh-libhelp\">Library Live Chat is being operated by Library staff</div>";
		} else {
		//<!-- Beginning of QuestionPoint qwidget code. --> 
			document.getElementById("live-chat").innerHTML = "<div class=\"chat-qp-lh-placeholder\"><div id=\"questionpoint.chatwidget1\" qwidgetno=\"1\" ></div></div><div class=\"chat-service-lh-qp\">Library Live Chat (Out of hours) is being operated by our partner institutions</div>"; 
		}
	}
	else {			
		if (showLibraryChat) {
		//<!-- Library Chat -->
			document.getElementById("live-chat").innerHTML = "<div class=\"chat-placeholder\"><div class=\"needs-js\">JavaScript disabled or chat unavailable. "+today+" "+tDay+"</div></div><div class=\"chat-service-libhelp\">Library Live Chat is being operated by Library staff</div>";
		} else {
		//<!-- Beginning of QuestionPoint qwidget code. --> 
			document.getElementById("live-chat").innerHTML = "<div class=\"chat-qp-placeholder\"><div id=\"questionpoint.chatwidget1\" qwidgetno=\"1\" ></div></div><div class=\"chat-service-qp\">Library Live Chat (Out of hours) is being operated by our partner institutions</div>"; 
		}
	}
}

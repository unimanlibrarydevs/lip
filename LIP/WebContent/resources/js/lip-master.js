//global vars - start...   
var all_locations = [
						'AGLC Floor -1',
						{name: 'AGLC Floor 1', areas: ['AGLC Floor 1 Oxford Rd.', 'AGLC Floor 1 Burlington St.', 'AGLC Floor 1 Break Out Space']},
						{name: 'AGLC Floor 2', areas: ['AGLC Floor 2 Oxford Rd.', 'AGLC Floor 2 Burlington St.', 'AGLC Floor 2 Break Out Space']},
						{name: 'AGLC Floor 3', areas: ['AGLC Floor 3 Oxford Rd.', 'AGLC Floor 3 Burlington St.', 'AGLC Floor 3 Break Out Space']},
						'Alan Turing G105 (School of Mathematics)',
						{name: 'Barnes Wallis', areas: ['Barnes Wallis D Floor (School of EEE)', 'Barnes Wallis Student Hub']},
						'Chemistry G43 (School of Chemistry)',
						{name: 'Ellen Wilkinson', areas: ['Ellen Wilkinson B3.1', 'Ellen Wilkinson B3.17', 'Ellen Wilkinson B3.3']},
						{name: 'George Begg', areas: ['George Begg B19 (School of MACE)', 'George Begg B7 (School of MACE)']},
						'George Kenyon Building',
						{name: 'Humanities Bridgeford Street', areas: ['Humanities Bridgeford Street 2.1', 'Humanities Bridgeford Street 2.2', 'Humanities Bridgeford Street 2.88']},
						'James Chadwick 3.007 (CEAS only)',
						'Joule Library',
						'Main Library Blue One',
						'Main Library Blue Two',
						'Main Library Blue Three',
						'Main Library Muriel Stott',
						'Mansfield Cooper 2.1',
						'Owens Park Main Building 1st Floor',
						{name: 'Pariser', areas: ['Pariser B30 (School of MACE)', 'Pariser B41 (B30 Annexe)']},
						'Precinct Library', 
						{name: 'Samuel Alexander', areas: ['Samuel Alexander Leamington', 'Samuel Alexander W2.19']}, 
						'Schuster Building M (School of Physics)',
						'Simon Building 6th Floor',
						{name: 'Stopford', areas: ['Stopford PC Cluster 1', 'Stopford PC Cluster 2', 'Stopford PC Cluster 3']},
						{name: 'The Mill A21', areas: ['The Mill A21 (School of CEAS)', 'The Mill B12 (School of CEAS)', 'The Mill C32 (School of CEAS)']},
						{name: 'Williamson', areas: ['Williamson 2.45 (School of EAES)', 'Williamson 3.33', 'Williamson 3.59', 'Williamson 4.06']}
                     ];

var all_pc_chart_settings = {
        chart: {
            type: 'bar',
            //backgroundColor: 'rgba(102,0,153,0.1)'
            //backgroundColor: '#dfdfff'
            backgroundColor: '#ffffff'
        },
        title: {
            text: '',
            style: {
                color : '#FFFFFF',
                fontSize: '24px',
                textShadow: 'none'
                }
        },
        xAxis: {
            categories: [
							'AGLC Floor -1',
							'AGLC Floor 1',
							'AGLC Floor 2',
							'AGLC Floor 3',
							'Alan Turing Building',
							'Barnes Wallis Building',
							'Chemistry Building',
							'Ellen Wilkinson Building',
							'George Begg Building',
							'George Kenyon Building',
							'Humanities Bridgeford Street',
							'James Chadwick Building',
							'Joule Library',
							'Main Library Blue One',
							'Main Library Blue Two',
							'Main Library Blue Three',
							'Main Library Muriel Stott',
							'Mansfield Cooper Building',
							'Owens Park Main Building',
							'Pariser Building',
							'Precinct Library', 
							'Samuel Alexander Building', 
							'Schuster Building',
							'Simon Building',
							'Stopford Building',
							'The Mill',
							'Williamson Building'
                     ],
            labels: {
                style: {
                    fontSize : '18px',
                    color: '#000000',
                    textShadow: 'none',
                    //fontWeight: 'normal'
                }
            }                   
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Campus PC Availability',
                //style: {'color': '#660099'},
                style:{
                    /*color: '#660099'*/
                    color: '#660099'
                },
                align: 'high'
            },
            labels: {
                style: {
                    fontSize : '18px',
                    color: '#000000',
                    //fontWeight: 'normal'
                }
            }                   
        },
        legend: {
        	enabled: false,
            align: 'left',
            verticalAlign: 'bottom', //BA: put legend at bottom
            y: 10, //BA: Move legend up
            floating: true,
            backgroundColor: '#bfbfbf',
            borderColor: '#CCC',
            borderWidth: 1,
            itemStyle: {
                fontSize : '18px',
                color: '#000000',
                textShadow: 'none'
            },                      
            shadow: false
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                pointWidth: 24, //BA: fix bar width to 20px...
                animation: false,
                dataLabels: {
                    align: 'center', //BA: center align
                    enabled: true,                          
                    /*style: {
                        fontSize: '1em'
                    },*/
                    style: {
                        fontSize : '16px',
                        textOutline: '0px',
                        //fontWeight: 'normal',
                        textShadow: 'none' //BA 'none' works, 'false' doesn't
                    },
                    //color: 'gray'  //BA: set data labels color
                    //color: '#FFCC33'
                    color: '#6e009d'
                    //color: 'yellow'
                },
		        point: {
		            events: {
		                click: function () {
		                    //alert('Category: ' + this.category + ', value: ' + this.y);
		                	var strCategory = this.category;
		                	var strId = "all_locations_ids." + strCategory + ".value";
		                	strBack = "#esc-pc-all-cluster-link";
		                	//console.log("strCategory = " + strCategory);
		                	//console.log("strId = " + strId);
		                	switch(this.category){
		                	case 'AGLC Floor -1':
		                	case 'AGLC Floor 1':
		                	case 'AGLC Floor 2':
		                	case 'AGLC Floor 3':
		                		strId = 386;
		                		break;
		                	case 'Alan Turing Building':
		                		strId = 44;
		                		break;
		                	case 'Barnes Wallis Building':
		                		strId = 113;
		                		break;
		                	case 'Chemistry Building':
		                		strId = 59;
		                		break;
		                	case 'Ellen Wilkinson Building':
		                		strId = 73;
		                		break;
		                	case 'George Begg Building':
		                		strId = 14;
		                		break;
		                	case 'George Kenyon Building':
		                		strId = 304;
		                		break;
		                	case 'Humanities Bridgeford Street':
		                		strId = 32;
		                		break;
		                	case 'James Chadwick Building':
		                		strId = 384;
		                		break;
		                	case 'Joule Library':
		                		strId = 207;
		                		break;
		                	case 'Main Library Blue One':
		                	case 'Main Library Blue Two':
		                	case 'Main Library Blue Three':
		                	case 'Main Library Muriel Stott':
		                		strId = 310;
		                		break;
		                	case 'Mansfield Cooper Building':
		                		strId = 323;
		                		break;
		                	case 'Owens Park Main Building':
		                		strId = 383;
		                		break;
		                	case 'Pariser Building':
		                		strId = 9;
		                		break;
		                	case 'Precinct Library':
		                		strId = 261;
		                		break;
		                	case 'Samuel Alexander Building':
		                		strId = 314;
		                		break;
		                	case 'Schuster Building':
		                		strId = 52;
		                		break;
		                	case 'Simon Building':
		                		strId = 57;
		                		break;
		                	case 'Stopford Building':
		                		strId = 313;
		                		break;
		                	case 'The Mill':
		                		strId = 11;
		                		break;
		                	case 'Williamson Building':
		                		strId = 322;
		                		break;		                		
		                	default:
		                		strId = null;
		                	}	                			                	
		                	//strUrl = 'http://www.manchester.ac.uk/discover/maps/interactive-map/embed/?pcclusters&id=' + strId;
		                	strUrl =  strMapSrc + '?pcclusters&id=' + strId;
		                	try{
			                	$('#esc-int-map-link').trigger('click');
			                	$('#esc-map').attr('src', strUrl);
		                	}
		                	catch(e){
		                		console.log("Error going to map from PC clusters...");
		                		//ignore...
		                	}
		                }
		            }
		        }        
            }
        },
        tooltip: {
            backgroundColor: '#bfbfbf',
            style: {
                color: '#000000',
                textShadow: 'none'                          
            }
        },
        colors: ['#999999', '#ffffff', '#6e009d', '#0d233a', '#8bbc21', '#910000', '#1aadce', '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'],                  
        //colors: ['#6e009d', '#ffffff', '#0d233a', '#8bbc21', '#910000', '#1aadce', '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'],                  
        //colors: ['#660099', '#ffffff', '#0d233a', '#8bbc21', '#910000', '#1aadce', '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'],                    
        series: [{
            name: 'Available',
            data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            }
       //}, {
       //     name: 'Occupied',
       // 	data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
       // }
        ]
    };                                  

//BA: set up context sensitive help - start
var esc_home_help = "Welcome to the Information Point.<br /><br />Click on an icon to do something. If you want to do something else, click the '<strong>Home</strong>' icon located centre top of the screen.<br /><br />Click on the help (<strong>?</strong>) icon in the top right hand corner of the screen to view help on the screen you're currently on.<br /><br />Use the '<strong>Home</strong>' icon (located centre top of the screen) to return to the home screen.";
var esc_int_map_help = "Search for the building you're looking for using the search bar.<ul><li>Use the filters on the left hand side to locate services.</li><li>Use pinch and stretch to zoom in and out.</li><li>You can tap on each building to see the name and location.</li></ul>Use the '<strong>Home</strong>' icon (located centre top of the screen) to return to the home screen.";
//var esc_pc_all_cluster_help = "This screen shows you all the PC clusters across campus. Purple shows the amount of computers in use and white shows the amount of computers that are free at each location.<br /><br />Use the '<strong>Home</strong>' icon (located centre top of the screen) to return to the home screen.";
var esc_pc_all_cluster_help = "This screen shows you all the PC clusters across campus in alphabetical order. The bars show the number of free computers available at each location.<br /><br />Use the '<strong>Home</strong>' icon (located centre top of the screen) to return to the home screen.";
var esc_lib_search_help = "If you're not sure where to start with Library Search use the online help or ask a member of staff for help.<br /><br />Use the '<strong>Home</strong>' icon (located centre top of the screen) to return to the home screen.";
var esc_find_book_help = "Follow our handy guide to help you find a book on the shelf. If you're still having difficulty, please ask a member of staff for more information.<br /><br />Use the '<strong>Home</strong>' icon (located centre top of the screen) to return to the home screen.";
var esc_contact_help = "We're always looking for ways to improve this app. Use this form to send feedback, comments, recommendations to the support team.<br /><br />Use the '<strong>Home</strong>' icon (located centre top of the screen) to return to the home screen.";
var esc_subject_guide_help = "This A to Z guide provides a list of subjects and their locations. Tap on the location to see the floor map.<br /><br />Use the '<strong>Home</strong>' icon (located centre top of the screen) to return to the home screen."; // CM: renamed from subject guide
var esc_plans_help = "These are the layouts for each floor. Find information about which books and services are available on each floor.<br /><br />You can use the colour coded navigation at the bottom of the screen to view another floor plan without having to go back to the home screen.<br /><br />Use the '<strong>Home</strong>' icon (located centre top of the screen) to return to the home screen.";
var esc_pc_clusters_help = "This screen shows you all the PC clusters across campus. The bars show the number of free computers that are available at each location.<br /><br />Use the '<strong>Home</strong>' icon (located centre top of the screen) to return to the home screen.";
var esc_opening_times_help = "These opening times will change depending on what time of year it is, remember to check back regularly.<br /><br />Use the '<strong>Home</strong>' icon (located centre top of the screen) to return to the home screen.";
var strCurrentHelp = esc_home_help;
//BA: set up context sensitive help - end

var intCurrentPageIndex = 0;
var objCurrentSwipeArray = null;
var arrFloorPlansMenu = ["#plans-page1", "#plans-page2"];
//var arrFloorPlans = ["#red2", "#red3", "#purpleG", "#purple4", "#orange4", "#orange5", "#green3", "#blue3FromStair1", "#blue3FromStair2", "#blue3FromStair3"];
var arrFloorPlans = ["#blueG", "#blue1", "#blue2", "#blue3", "#blue4", "#green3", "#orange4", "#orange5", "#purpleG", "#purple2", "#purple4", "#red2", "#red3"];
var arrPcClustersMenu = ["#pc-clusters-page1"];
var arrPcClusters = ["#esc-pc-all-cluster", "#esc-aglc-cluster", "#esc-ml-cluster"];

var classifications = []; // CM: list of subject-location classifications
var sheetKey = '1jK7fe2BF5H_vZedFysCSEabqaI6L5UZqzb3d99pHm3Y'; // CM: key for public Google sheet holding subject-location data 

var isZebra = false; //BA: used to give dynamic tables zebra effect... 

var strBack = ""; // stores if to show back button and where to go. "" indicates back button not to be shown.

var returnHome = null; //BA: 27/02/17 - Use this to store the setTimeout handle to return home after 3 mins of being on a screen other than home screen.
var strSearchSrc = "https://man-fe.hosted.exlibrisgroup.com/primo-explore/search?vid=MU_VU1_BETA&lang=en_US&sortby=rank";  //Search iframe src
//var strQuaireSrc = "https://manchesterlib.eu.qualtrics.com/jfe/form/SV_cGdhvw0LmBQa6BT"; //Questionnaire iframe src
var strQuaireSrc = "https://manchesterlib.eu.qualtrics.com/jfe/form/SV_1HujoA1HzBP2ov3"; //Questionnaire iframe src
var strMapSrc = "http://www.manchester.ac.uk/discover/maps/interactive-map/embed/"; //Interactive map src
var alertDialog;
var loadingDialog;
//test if mobile device - start
var isMobile = false;
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	 isMobile = true;
	}
//test if mobile device - end
var isIos = false;

//detect iOS
if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
	isIos = true;
}
//global vars - end...

//webshims.polyfill('forms forms-ext');                                     

$(document).ready(function(){   
	
    //Google Analytics - start
    //using personal gmail account...
/*                 
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
                
                  ga('create', 'UA-77459095-1', 'auto');
                  ga('send', 'pageview');
 */
	//BA: This uses the DLADT.UoM@gmail.com account (pointing to Library Information Point) 
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-83851118-1', 'auto');
	  ga('send', 'pageview');
 
                // Google Analytics - end

    //BA: 27/02/2017 - prevent Highcharts credit from navigating away from window - start
    try{
	  Highcharts.setOptions({
        credits: {
          //text: 'test credits'
        	href: 'javascript:this.preventDefault;'
        }
      });
    }
    catch(e){
    	//ignore...
    }
    //BA: 27/02/2017 - prevent Highcharts credit from navigating away from window - end
    try{
    	$('#esc-pc-all').highcharts(all_pc_chart_settings);
    }
    catch(e){
    	//ignore
    }
	  
    //container.init();
    loadingDialog = new LoadingDialog('#loading-dialog');
    alertDialog = new AlertDialog('#alert-dialog', '#message-div')
    //loadingDialog.init();
    //alertDialog.init();
    //Turn off ajax caching...
    $.ajaxSetup({cache: false});
            
    $('.check-validity').on('click', function () {
        $(this).jProp('form').checkValidity();
        return false;
    });

    
    $('.esc-icon, .esc-nav, #esc-questionnaire-link').on('click', function(){ //BA: temporary questionnaire hack
    //$('.esc-icon, .esc-nav').on('click', function(){
        //$('#esc-quaire').attr('src', $('#esc-quaire').attr('src')); //BA: temporary questionnaire hack    	
        var strDivId = "#" + $(this).attr("rel");
        var strLinkId = "#" + $(this).attr("id") + "-link";
        
        if(strDivId != "#esc-help"){
            $('.esc-widget').hide();
            //BA: set page sensitive help - start
            var strCurrentHelpId = $(this).attr("rel").replace(/-/g, "_");
            strCurrentHelpId += "_help";
            try{
                strCurrentHelp = eval(strCurrentHelpId);                            
            }
            catch(e){
                //ignore for now...
                //alert(e.message);
            }
            //BA: set page sensitive help - end
        }
        $(strDivId).show();                     
        // BA: 09/05/16 - google analytics tracking - start
        ga('set', 'page', strDivId);
        ga('send', 'pageview');
        // BA: 09/05/16 - google analytics tracking - end                   
        
        $(strLinkId).hide();
        //console.log("Div Id = " + strDivId);                  
        try{
            if((strDivId != "#esc-help") && (alertDialog.isOpen())){
                alertDialog.close();
            }                           	
        }
        catch(e){
        	//ignore
        }
        if(strDivId == "#esc-subject-guide"){
            $('.esc-guide').show();
            strBack = "#esc-subject-guide";
        }
        if($(this).hasClass("esc-logo") && (strDivId != "#esc-help")){
            $('#esc-main-logo').show();
        }
        else if(strDivId != "#esc-help"){
            $('#esc-main-logo').hide();
        }
        if(strDivId == "#esc-home"){
        	
            //console.log("home button clicked....");
        	$('#esc-questionnaire').hide(); //BA: temporary questionnaire hack
            $('#esc-home').show();
            intCurrentPageIndex = 0;                        
            //BA: handle floor plans - start
            $('#esc-floor-plans-widgets').show();
            $('#esc-floor-plan').val("none");
            $('.esc-floor-plan').hide(); // hide all floor plans
            //BA: handle floor plans - end

            //BA: handle pc clusters - start
            $('#esc-pc-clusters-widgets').show();
            $('#esc-pc-cluster').val("none");
            $('.esc-pc.cluster').hide(); // hide all floor plans
            //BA: handle pc cluster - end

            //BA: make sure iframes always show what they are supposed to.
            //$('#esc-quaire').attr('src', $('#esc-quaire').attr('src')); //BA: temporary questionnaire hack
/*
            if($('#esc-quaire').attr('src') != strQuaireSrc){
            	console.log("resetting quaire src...");
            	$('#esc-quaire').attr('src', strQuaireSrc);
            }            
            //$('#esc-map').attr('src', $('#esc-map').attr('src'));
            if($('#esc-map').attr('src') != strMapSrc){
            	console.log("resetting map src...");
                $('#esc-map').attr('src', strMapSrc);            
                //$('#esc-map').attr('src', 'http://www.manchester.ac.uk/discover/maps/interactive-map/embed/?id=100');            	
            }
            //BA: 17/07/2017 - Fix bug where keypad pops up evertime after using library search - start            
            if($('#esc-search').length && ($('#esc-search').attr('src') != strSearchSrc)){
            	console.log("resetting search src...");
            	$('#esc-search').attr('src', strSearchSrc);
            }
*/            
            //$('#esc-search').attr('src', $('#esc-search').attr('src'));
            //$('#esc-search').contents().find('#search_field').val('');
            //$('#esc-search').contents().find('.EXLClearSimpleSearchBoxButtonClose').trigger('click');            
            //$('#esc-search').contents().find('#submit').trigger('click');
            //$('#esc-search').contents().find('.EXLResultsContainer').hide();            
            //BA: 17/07/2017 - Fix bug where keypad pops up evertime after using library search - end
            
            //$('#esc-room').attr('src', $('#esc-room').attr('src'));
            //$('#esc-search').contentWindow.location.reload();
            //$('#esc-search').contentDocument.location.reload(true);
			//BA: hide nav widgets...
            $('.esc-nav-widgets').hide();
            //BA: 21/07/16 - reset Back button display
            strBack = "";
            $('.esc-back').hide();
        	$('#books-img').show();
        	$('#books-text').hide();
            
        	//reset opening times - start
        	//$('.symbol').html('+');
        	$('.opening-time-detail').hide();
        	//reset opening times - end
        	
        	//reset library chat - start
        	//if($('.uom_liveChat_container').is(':visible')){
        	if(!$('.uom_liveChat_container').hasClass('hide')){
        		$('.uom_liveChat_container').addClass('hide');
        	}
        	
        	$('.uom_liveChat_container').show();
        	        	
        	//reset library chat - end
        	
/*            
        	setTimeout(function() {
            	try{
            		//alert("blurr here...");
            		//$(document).activeElement.trigger('blur');
            		$('body').trigger('click');
            	}
            	catch(e){
            		//ignore
            	}
            }, 5000);
*/        	
        }
        else if(strDivId == "#esc-plans"){
            $('#esc-home').hide();
            objCurrentSwipeArray = arrFloorPlansMenu;
            intCurrentPageIndex = 0;
            $('.esc-floor-plans-menu').hide();
            $(objCurrentSwipeArray[intCurrentPageIndex]).show();    

            $('.esc-nav-widgets').hide(); // hide page widgets at bottom of page
            var tmpId = "#" + "esc-plans-widgets-page" + intCurrentPageIndex;
            $(tmpId).show(); // show relevant page widgets at bottom of page

            //BA: 21/07/16 - set Back button display
            if(strBack != ""){
                $('.esc-back').show();
            }
            else{
                $('.esc-back').hide();
            }
            $(document).scrollTop(0); // scroll to top of page
        }
        else if(strDivId == "#esc-pc-clusters"){
        	strBack = "#esc-pc-all-cluster-link";
            //console.log("#pc-clusters click...");
            $('#esc-home').hide();
            objCurrentSwipeArray = arrPcClustersMenu;
            intCurrentPageIndex = 0;
            $('.esc-pc-clusters-menu').hide();
            $(objCurrentSwipeArray[intCurrentPageIndex]).show();    

            $('.esc-nav-widgets').hide(); // hide page widgets at bottom of page
            var tmpId = "#" + "esc-pc-clusters-widgets-page" + intCurrentPageIndex;
            $('.esc-pc-clusters-widgets').show();
            //console.log("tmpId = " + tmpId);
            $(tmpId).show(); // show relevant page widgets at bottom of page                                                                
        }
        else if(strDivId == "#esc-contact"){
            $('#esc-home').hide();
            $('.esc-nav-widgets').hide();
        }
        else if(strDivId == "#esc-help"){
            try{
                alertDialog.open(strCurrentHelp, "Help");
            }
            catch(e){
                //ignore for the time being...
            }
        }
        else{
            $('#esc-home').hide(); 
            //$('#esc-main-logo').hide();
        }
        
        //BA: 27/02/2017 - if not on home page, return to home page after 3 mins inactivity - start
        if(returnHome != null){
        	clearTimeout(returnHome);
        }
        
        returnHome = setTimeout(function(){
        	if(!($('#esc-home').is(':visible'))){
        		$('.esc-home').trigger('click');
        		clearTimeout(returnHome);
        		returnHome = null;
        		$('#esc-help-img').trigger('click'); //BA; show the help screen when automatically returning to home screen.
            	$('#esc-back').hide(); //BA: Hide back button
        	}
        }, 180000);
        //}, 18000000); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Revert this after fixing for mobile phone
        //BA: 27/02/2017 - if not on home page, return to home page after 3 mins inactivity - end
        
        //BA: 11/07/17 - Hide keyboard on tablet devices - start
        if($(this).attr('rel') != 'esc-pc-all-cluster'){
        	try{
                //$(document).activeElement.blur();
        		//$(':focus').trigger('blur');
        		$(document).activeElement.trigger('blur');
        	}
        	catch(e){
        		//ignore
        	}
        }

    	//console.log("strDivId = " + strDivId);
        if(strDivId != "#esc-home"){
	        if($('#esc-home').is(':visible')){
	        	$('#esc-back').hide();
	        }
	        else{
	        	$('#esc-back').show();
	        }
        }
        else{
        	$('#esc-back').hide();        	
        }
        
        //BA: 11/07/17 - Hide keyboard on tablet devices - end
        //$(this).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
/*        
        $('type=["text"], textarea').attr('readonly', 'readonly'); // Force keyboard to hide on input field.
        $('type=["text"], textarea').attr('disabled', 'disabled'); // Force keyboard to hide on textarea field.
        setTimeout(function() {
        	$('type=["text"], textarea').blur();  //actually close the keyboard
            // Remove readonly attribute after keyboard is hidden.
        	$('type=["text"], textarea').removeAttr('readonly');
        	$('type=["text"], textarea').removeAttr('disabled');
       		$(document).activeElement.trigger('blur');
        }, 5000);
*/       
        if(strDivId == '#esc-lib-search'){
        	//console.log("hide.....");
        	$('.uom_liveChat_container').hide();        	
        }
        else{
        	//console.log("show.....");
        	$('.uom_liveChat_container').show();
        }

        //BA: 14/09/17 - try only set iframe sources when needed - start         
        if(strDivId == '#esc-questionnaire'){
        	//console.log("resetting quaire src...");
        	$('#esc-quaire').attr('src', strQuaireSrc);
        }            
        //$('#esc-map').attr('src', $('#esc-map').attr('src'));
        if(strDivId == '#esc-int-map'){
        	//console.log("resetting map src...");
            $('#esc-map').attr('src', strMapSrc);            
        }
        //BA: 17/07/2017 - Fix bug where keypad pops up evertime after using library search - start            
        if(strDivId == '#esc-lib-search'){
        	//console.log("resetting search src...");
        	$('#esc-search').attr('src', strSearchSrc);
        }
        //BA: 14/09/17 - try only set iframe sources when needed - end                 
        
    });                 

    //$('tspan').attr('onclick', 'javascript:this.preventDefault;');
    
    $('#esc-reset').on('click', function(){
        $('.esc-form-field').val('');
    });
    
    $('.esc-a-z').on('click', function(){
        var strId = $(this).attr("id").replace("esc-", "");
        var strClass = "." + strId;
        //console.log("strId = " + strId);
        $('.esc-guide').hide();
        $(strClass).show();
    });
    
    $('#esc-all').on('click', function(){
        $('.esc-guide').show();
    });
                    
    $('body').on('click','.esc-floor-link', function(){ // CM: replaced $('.class').on('event'...) to trigger on not-yet-existing elements
    //$('.esc-floor-link').on('click', function(){  //BA: PS: This works fine with dynamically created (not yet created) elements of this class...
        //console.log("here...");
        $('#esc-pc-clusters-widgets').hide();
        $('#esc-floor-plans-widgets').hide();
        $('#esc-subject-guide').hide();
        var strDivId = "#" + $(this).attr('rel');
        var strRel = $(this).attr('rel');
        $('.esc-floor-plan').hide();
        $('#esc-plans').show();
        $(strDivId).show();
        
        //BA: 11/07/16 - show placeholder text - start
        tmp = arrFloorPlans.toString();
        if(tmp.indexOf(strDivId) < 0){
            $('#placeholder').show();
        }
        
        //BA: 11/07/16 - show placeholder text - end
        
        // BA: 20/05/16 - google analytics tracking - start
        ga('set', 'page', strDivId);
        ga('send', 'pageview');
        // BA: 20/05/16 - google analytics tracking - end                   
        //set current swipe array
        objCurrentSwipeArray = arrFloorPlans;
        //set current page index
        var x = 0;
        var boolFound = false;
        while((x < objCurrentSwipeArray.length - 1) && (!boolFound)){
            if(objCurrentSwipeArray[x] == strDivId){
                intCurrentPageIndex = x;
                boolFound = true;
            }
            x++;
        }
        $('.esc-nav-widgets').hide(); // hide page widgets at bottom of page
        //BA: 21/07/16 - show/hide back button
        if(strBack != ""){
            $('.esc-back').show();
        }
        else{
            $('.esc-back').hide();
        }
        //BA: show floor plan widgets...
        $('.esc-floor-plan-widgets').show();
        //$('.esc-floor-plan-widget').removeClass('esc-active-widget');
        var strTmp = ".esc-floor-plan-widget[rel=" + strRel + "]";
        //$(strTmp).addClass('esc-active-widget');
        //console.log("do stuff here...");
        //$('.esc-floor-plan-widget').css("font-size", "0.8em");
        $('.esc-floor-plan-widget').css("font-size", "1em");
        //$(strTmp).css("font-size", "1.2em");
        $(strTmp).css("font-size", "2em");
    });

    $('.esc-pc-cluster-link').on('click', function(){
        //console.log("here 222...");
        $('#esc-pc-clusters-widgets').hide();
        $('#esc-floor-plans-widgets').hide();
        $('#esc-subject-guide').hide();
        var strDivId = "#" + $(this).attr('rel');
        var strRel = $(this).attr('rel');
        $('.esc-pc-cluster').hide();
        $('#esc-pc-clusters').show();
        //console.log("strDivId = " + strDivId);
        $(strDivId).show();
        // BA: 20/05/16 - google analytics tracking - start
        ga('set', 'page', strDivId);
        ga('send', 'pageview');
        // BA: 20/05/16 - google analytics tracking - end                   
        //$(strDivId).parent().show();
        //set current swipe array
        objCurrentSwipeArray = arrPcClusters;
        //set current page index
        var x = 0;
        var boolFound = false;
        while((x < objCurrentSwipeArray.length - 1) && (!boolFound)){
            if(objCurrentSwipeArray[x] == strDivId){
                intCurrentPageIndex = x;
                boolFound = true;
            }
            x++;
        }
        $('.esc-nav-widgets').hide(); // hide page widgets at bottom of page
        //BA: show floor plan widgets...
        //$('.esc-pc-cluster-widgets').show();
        $('.esc-pc-cluster-widget').removeClass('esc-active-widget');
        var strTmp = ".esc-pc-cluster-widget[rel=" + strRel + "]";
        //console.log("strTmp = " + strTmp);
        $(strTmp).addClass('esc-active-widget');
    });
    
    //BA: Handle back button click separately - start
    $('#esc-back').on('click', function(){
    	//console.log("strBack =" + strBack);
    	if($('#esc-pc-all-cluster').is(':visible')){
    		strBack = "";
    		//console.log("about to trigger home button click...");
    		$('.esc-home').trigger('click');    		
    	}
    	else if($('#esc-opening-times').is(':visible')){
    		strBack = "";
    		$('.esc-home').trigger('click');    		
    	}
    	else if($('#esc-subject-guide').is(':visible')){
    		strBack = "";
    		$('.esc-home').trigger('click');    		
    	}
    	else if(strBack == '#esc-pc-all-cluster-link'){
    		//console.log("about to trigger pc-cluster-link click...");
    		$('#esc-int-map').hide();
    		//$('#esc-pc-all-cluster-link').trigger('click');
    		$('#esc-pc-all-cluster').show();
    		$('#esc-pc-clusters').show();
    	}
    	else if(strBack == '#esc-opening-times-link'){
    		$('#esc-opening-times-link').trigger('click');
    	}
    	else if(strBack == '#esc-subject-guide'){
    		$('#esc-subject-guide-link').trigger('click');
    	}
    	else{
    		$('.esc-home').trigger('click');
    	}
    });
    //BA: Handle back button click separately - end
    
    
    //BA: 27/02/2017 - disable swipe
    //$('.esc-swipe').on("swiperight", function(event){
    $('.esc-swipe').on("swiperightDISABLED", function(event){
        //console.log("swipe right...");
        if($(this).hasClass('esc-floor-plans-menu')){
            objCurrentSwipeArray = arrFloorPlansMenu;
            $('.esc-floor-plans-menu').hide();
        }
        else if(($(this).hasClass('esc-floor-plan')) || ($(this).parent().hasClass('esc-floor-plan'))){
            objCurrentSwipeArray = arrFloorPlans;
            $('.esc-floor-plan').hide();
        }
        else if($(this).hasClass('esc-pc-cluster')){
            objCurrentSwipeArray = arrPcClusters;
            $('.esc-pc-cluster').hide();
        }
        if(intCurrentPageIndex == 0){
            intCurrentPageIndex = objCurrentSwipeArray.length - 1;
            $(objCurrentSwipeArray[intCurrentPageIndex]).show();                            
        }
        else{
            intCurrentPageIndex--;
            $(objCurrentSwipeArray[intCurrentPageIndex]).show();                            
        }
        // BA: 20/05/16 - google analytics tracking - start
        ga('set', 'page', objCurrentSwipeArray[intCurrentPageIndex]);
        ga('send', 'pageview');
        // BA: 20/05/16 - google analytics tracking - end                   

        $('.esc-nav-widgets').hide(); // hide page widgets at bottom of page                    
        //BA: show plans widgets appropriately - start
        if(($(this).hasClass('esc-floor-plan')) || ($(this).hasClass('esc-floor-plans-menu')) || ($(this).parent().hasClass('esc-floor-plan'))){
        //if(($(this).hasClass('esc-floor-plan')) || ($(this).hasClass('esc-floor-plans-menu')) || ($(this).parent().hasClass('esc-floor-plans-menu')) || ($(this).parent().hasClass('esc-floor-plan'))){
            //BA: show floor plan widgets...
            var strRel = objCurrentSwipeArray[intCurrentPageIndex].replace("#", "");
            $('.esc-floor-plan-widgets').show();
            //$('.esc-floor-plan-widget').removeClass('esc-active-widget');
            var strTmp = ".esc-floor-plan-widget[rel=" + strRel + "]";
            //$(strTmp).addClass('esc-active-widget');                                              
            //$('.esc-floor-plan-widget').css("font-size", "0.8em");
            $('.esc-floor-plan-widget').css("font-size", "1em");
            //$(strTmp).css("font-size", "1.2em");
            $(strTmp).css("font-size", "2em");
            
            if($(this).hasClass('esc-floor-plans-menu')){
                var tmpId = "#" + "esc-plans-widgets-page" + intCurrentPageIndex;
                $(tmpId).show(); // show relevant page widgets at bottom of page                            
            }
            //console.log ("should have shown " + tmpId);
            
            //BA: set page sensitive help - start
            strCurrentHelpId = "esc_plans_help";
            //console.log("swiperight 1...: strCurrentHelpId = " + strCurrentHelpId);
            try{
                strCurrentHelp = eval(strCurrentHelpId);
                $('#message-div').html(eval(strCurrentHelpId));
            }
            catch(e){
                //ignore for now...
                //alert(e.message);
            }
            //BA: set page sensitive help - end
            
        }
        else if($(this).hasClass('esc-pc-cluster')){
            //BA: show floor plan widgets...
            var strRel = objCurrentSwipeArray[intCurrentPageIndex].replace("#", "");
            $('.esc-pc-cluster-widgets').show();
            $('.esc-pc-cluster-widget').removeClass('esc-active-widget');
            var strTmp = ".esc-pc-cluster-widget[rel=" + strRel + "]";
            $(strTmp).addClass('esc-active-widget');                        
            //var tmpId = "#" + "esc-pc-clusters-widgets-page" + intCurrentPageIndex;
            //$(tmpId).show(); // show relevant page widgets at bottom of page
            
            //BA: set page sensitive help - start
                strCurrentHelpId = strRel.replace(/-/g, '_') + "_help";
                //console.log("swiperight 2...: strCurrentHelpId = " + strCurrentHelpId);
                try{
                    strCurrentHelp = eval(strCurrentHelpId);                            
                    $('#message-div').html(eval(strCurrentHelpId));
                }
                catch(e){
                    //ignore for now...
                    //alert(e.message);
                }
            //BA: set page sensitive help - end
        }
        //BA: show plans widgets appropriately - end
    })

    //BA: 27/02/2017 - disable swipe
    //$('.esc-swipe').on("swipeleft", function(event){
    $('.esc-swipe').on("swipeleftDISABLED", function(event){
        //console.log("swipeleft");
        if($(this).hasClass('esc-floor-plans-menu')){
            objCurrentSwipeArray = arrFloorPlansMenu;
            $('.esc-floor-plans-menu').hide();
        }
        else if(($(this).hasClass('esc-floor-plan')) || ($(this).parent().hasClass('esc-floor-plan'))){
            objCurrentSwipeArray = arrFloorPlans;
            $('.esc-floor-plan').hide();
        }
        else if($(this).hasClass('esc-pc-cluster')){
            objCurrentSwipeArray = arrPcClusters;
            $('.esc-pc-cluster').hide();
        }
        if(intCurrentPageIndex == objCurrentSwipeArray.length - 1){
            intCurrentPageIndex = 0;
            $(objCurrentSwipeArray[intCurrentPageIndex]).show();
            //$("body").pagecontainer("change", objCurrentSwipeArray[intCurrentPageIndex], { options });                        
        }
        else{
            intCurrentPageIndex++;
            $(objCurrentSwipeArray[intCurrentPageIndex]).show();
            //$(this).pagecontainer("change", objCurrentSwipeArray[intCurrentPageIndex], { options });                                              
        }
        
        // BA: 20/05/16 - google analytics tracking - start
        ga('set', 'page', objCurrentSwipeArray[intCurrentPageIndex]);
        ga('send', 'pageview');
        // BA: 20/05/16 - google analytics tracking - end                   
        
        $('.esc-nav-widgets').hide(); // hide page widgets at bottom of page
        //console.log("left - index = " + intCurrentPageIndex);
        
        //BA: show plans widgets appropriately - start
        if(($(this).hasClass('esc-floor-plan')) || ($(this).hasClass('esc-floor-plans-menu')) || ($(this).parent().hasClass('esc-floor-plan'))){
        //if(($(this).hasClass('esc-floor-plan')) || ($(this).parent().hasClass('esc-floor-plan'))){
            //BA: show floor plan widgets...
            var strRel = objCurrentSwipeArray[intCurrentPageIndex].replace("#", "");
            $('.esc-floor-plan-widgets').show();
            //$('.esc-floor-plan-widget').removeClass('esc-active-widget');
            var strTmp = ".esc-floor-plan-widget[rel=" + strRel + "]";
            //$(strTmp).addClass('esc-active-widget');                      
            //$('.esc-floor-plan-widget').css("font-size", "0.8em");
            $('.esc-floor-plan-widget').css("font-size", "1em");
            //$(strTmp).css("font-size", "1.2em");
            $(strTmp).css("font-size", "2em");
            
            if($(this).hasClass('esc-floor-plans-menu')){
                var tmpId = "#" + "esc-plans-widgets-page" + intCurrentPageIndex;
                $(tmpId).show(); // show relevant page widgets at bottom of page                            
            }
            //BA: set page sensitive help - start
            strCurrentHelpId = "esc_plans_help";
            //console.log("swiperight 1...: strCurrentHelpId = " + strCurrentHelpId);
            try{
                strCurrentHelp = eval(strCurrentHelpId);                            
                $('#message-div').html(eval(strCurrentHelpId));
            }
            catch(e){
                //ignore for now...
                //alert(e.message);
            }
            //BA: set page sensitive help - end                         
        }
        else if($(this).hasClass('esc-pc-cluster')){
            //BA: show floor plan widgets...
            var strRel = objCurrentSwipeArray[intCurrentPageIndex].replace("#", "");
            $('.esc-pc-cluster-widgets').show();
            $('.esc-pc-cluster-widget').removeClass('esc-active-widget');
            var strTmp = ".esc-pc-cluster-widget[rel=" + strRel + "]";
            $(strTmp).addClass('esc-active-widget');                        
            //var tmpId = "#" + "esc-pc-clusters-widgets-page" + intCurrentPageIndex;
            //$(tmpId).show(); // show relevant page widgets at bottom of page  

            //BA: set page sensitive help - start
            strCurrentHelpId = strRel.replace(/-/g, '_') + "_help";
            //console.log("swiperight 2...: strCurrentHelpId = " + strCurrentHelpId);
            try{
                strCurrentHelp = eval(strCurrentHelpId);                            
                $('#message-div').html(eval(strCurrentHelpId));
            }
            catch(e){
                //ignore for now...
                //alert(e.message);
            }
            //BA: set page sensitive help - end                         
        }
        //BA: show plans widgets appropriately - end
        
    });
    
    $('.esc-floor-plans-page-widget').on('click', function(){
        objCurrentSwipeArray = arrFloorPlansMenu;
        intCurrentPageIndex = $(this).attr('rel');
        $('.esc-floor-plans-menu').hide();
        $(objCurrentSwipeArray[intCurrentPageIndex]).show();
        
        $('.esc-nav-widgets').hide(); // hide page widgets at bottom of page
        var tmpId = "#" + "esc-plans-widgets-page" + intCurrentPageIndex;
        $(tmpId).show(); // show relevant page widgets at bottom of page                                        
        
    });

    // CM: get live subject location data and build page dynamically - start            
    $.getJSON('https://spreadsheets.google.com/feeds/list/'+sheetKey+'/od6/public/values?alt=json-in-script&callback=?', function(data) { // after resources loaded
        //BA: 20/05/16 - remove static content from locations table prior to populating with dynamic data..
        $('#subLocData').empty();
    
        //BA: 07/06/16 - Add the header row - start
        var subLocHeaderHTML = '<thead>'
                    + '<tr>'
                    + '<th>Subject</th>'
                    + '<th>Classification</th>'
                    + '<th>Book Location</th>'
                    + '</tr>'
                + '</thead>';
        $('#subLocData').append(subLocHeaderHTML);
        //BA: 07/06/16 - Add the header row - end
        
        isZebra = false; //BA: re-initialise isZebra
    
        // CM: parse the json object into an array for later reference, while appending it to the page
        $.each(data.feed.entry, function(i,row){
            var thisRow = {
                'subject': row.gsx$subject.$t,
                'fileUnder': row.gsx$subject.$t[0].toUpperCase() + row.gsx$subject.$t[0].toLowerCase(),
                'classification': row.gsx$classification.$t.split(','),
                'locations': $.grep([ row.gsx$location1.$t, row.gsx$location2.$t, row.gsx$location3.$t ], function(k,i){ return k!='' })
            };
            
            classifications.push(thisRow);

            // CM: create temporary list of location HTML fragments 
            locationsHTML = [];
            $.each(thisRow.locations,function(i,row){
                locId = row.replace(' ','').split(' ')[0];
                locId = locId[0].toLowerCase() + locId.substring(1);
                // console.log(row, locId);
                locationsHTML.push('<a href="#" title="'+row+'" rel="'+locId+'" class="esc-floor-link">'+row+'</a>');
            }); 

            // CM: create HTML fragment for this subject row
            var subLocHTML;
            //BA: 08/06/16 - Add zebra effect to table
            if(isZebra){
                subLocHTML = '<tr style="display:table-row;" class="esc-zebra esc-guide '+thisRow.fileUnder+'">'
                +'<td class="mobile-device">'+thisRow.subject+'&nbsp;</td>'
                +'<td class="mobile-device">'+thisRow.classification.join(', ')+'&nbsp;</td>'
                +'<td>'+locationsHTML.join(', ')+'&nbsp;</td>'
                +'</tr>';                           
            }
            else{
                subLocHTML = '<tr style="display:table-row;" class="esc-guide '+thisRow.fileUnder+'">'
                +'<td class="mobile-device">'+thisRow.subject+'&nbsp;</td>'
                +'<td class="mobile-device">'+thisRow.classification.join(', ')+'&nbsp;</td>'
                +'<td>'+locationsHTML.join(', ')+'&nbsp;</td>'
                +'</tr>';
                
            }
            isZebra = !isZebra;
            // CM: append the subject row to the page
            $('#subLocData').append(subLocHTML);
        }); 

    }); 
    // CM: get subject location data from public source and build page dynamically - end            
    
    $('.esc-back').on('click', function(){
        $('#esc-plans-widgets').hide();
        $('#esc-subject-guide-link').trigger('click');
    });
    
    
    function refresh() {
        //console.log("refresh...");
        $.get('http://www.itservices.manchester.ac.uk/clusteravailability/avail.php').done(function(xml) {
            //AGLC locations...
            //campus locations...
            all_pc_chart_settings.series[0].data = [];
            //all_pc_chart_settings.series[1].data = [];
            $(all_locations).each(function(all_index, all_location){
                if(typeof all_location == 'object') {
                    var all_name, all_avail, all_total, all_open;
                    //var all_clusterNode = $(xml).find('PLPlace name:contains("' + all_location.areas[0] + '")').parent();
                    var all_clusterNode = $(xml).find('PLPlace name:contains("' + all_location.name + '")').parent();
                    //console.log("area = " + all_location.name);
                    all_name = $(all_clusterNode).find('name').text();
                    all_open = all_clusterNode.find('open').text() == 'true';
                    all_avail = 0;
                    all_total = 0;
                    $.each(all_location.areas, function(i, all_locationArea) {
                        //console.log("all location area = " + all_locationArea);
                        var all_clusterNode = $(xml).find('PLPlace name:contains("' + all_locationArea + '")').parent();
                        var all_numbers = all_clusterNode.find('availability').text().split(',');
                        //console.log("avail = " + parseInt(all_numbers[1]));
                        //console.log("all_numbers = " + all_numbers);
                        if($.isNumeric(parseInt(all_numbers[1]))){
                        	//console.log("isNumeric = " + $.isNumeric(all_numbers));
                            all_avail += parseInt(all_numbers[1]);
                            all_total += parseInt(all_numbers[0]) + parseInt(all_numbers[1]);                        	
                        }
                    });
                    //console.log("all_avail = ");
                    //console.dir(all_avail);
                    all_pc_chart_settings.series[0].data.push(all_avail);
                    //all_pc_chart_settings.series[1].data.push(all_total - all_avail);                                                     
                } 
                else {
                    var all_name, all_avail, all_total, all_open;
                    var all_clusterNode = $(xml).find('PLPlace name:contains("' + all_location + '")').parent();
                    all_name = $(all_clusterNode).find('name').text();
                    all_open = all_clusterNode.find('open').text() == 'true';
                    var all_numbers = all_clusterNode.find('availability').text().split(',');
                    all_avail = parseInt(all_numbers[1]);
                    all_total = parseInt(all_numbers[0]) + all_avail;
                    all_pc_chart_settings.series[0].data.push(all_avail);
                    //all_pc_chart_settings.series[1].data.push(all_total - all_avail);                                                     
                }                                                       
            });
            try{
	            $('#esc-pc-all').highcharts(all_pc_chart_settings);
	            $('#esc-last-updated-all-input').html(getCurrentDatetime());
	            //loadingDialog.close();
	            $('#esc-pc-all').show();
            }
            catch(e){
            	//ignore
            }
        });
    }                               
    
    $('#esc-pc-all').html("<div style=\"text-align: center;\">loading...</div>");
    refresh();
    //$('#esc-pc-all').hide();
    //alert("hide...");
    //loadOpeningTimes();
                    
    window.setInterval(function(){
        refresh();                  
        //loadOpeningTimes();
    },300000);
    
    $('#esc-fill').on('click', function(){
        $('#esc-name').val("User Name");
        $('#esc-email').val("Berrisford.Edwards@manchester.ac.uk");
        $('#esc-comments').val("This is my feedback comment...");
    });
    
    $('#esc-submit').on('click', function(){
        //BA: 07/06/16 - Check that all required fields have been completed before submitting form...
        var objForm = document.getElementById('esc-feedback-form');
        if(objForm.checkValidity()){                                            
            //Just send the data to LanDesk, don't check for success, reset the form and inform the user...
        	//https://supportcentre.manchester.ac.uk/ServiceDesk.EventManager/SendMessage.aspx?
            $.ajax({
                //url: "https://supportcentre.manchester.ac.uk/Dev.EventManager/SendMessage.aspx?source=UoM Web Form Request&type=New Request&title=Library Information Point Feedback&desc=" + $('#esc-comments').val() + "&ci=Self Service Guest&p1=" + $('#esc-email').val() + "&p2=A General IT Services Request&p3=Man Medical School&p5=Applications - Corporate Applications - Medlea&p7=True&p8=" + $('#esc-name').val() + "&p9=Building not specified&p10=12345&p11=Other",
                url: "https://supportcentre.manchester.ac.uk/ServiceDesk.EventManager/SendMessage.aspx?source=UoM Web Form Request&type=New Request&title=Information Point Feedback&desc=" + $('#esc-comments').val() + "&ci=Self Service Guest&p1=" + $('#esc-email').val() + "&p2=A General IT Services Request&p3=Man Medical School&p5=Applications - Corporate Applications - Medlea&p7=True&p8=" + $('#esc-name').val() + "&p9=Building not specified&p10=12345&p11=Other",
                crossDomain: true,
                error:function(){
                	console.log("ERROR sending feedback");
                },
                success:function(){
                	console.log("Feedback success...");
                }
            });
            try{
                alertDialog.open("<h1>Thank you</h1><p>Your feedback has been sent.</p><p>You should normally receive a reply within five working days.</p>", "Feedback Sent");            	
            }
            catch(e){
            	console.log("Try-catch error...");
            }
            //$('#esc-reset').trigger('click');
            $('.esc-form-field').val('');

            //BA: 27/02/17 - return to home screen after 3 seconds - start
            setTimeout(function(){
            	//alertDialog.close();
                $('.esc-form-field').val('');
                $('.esc-home').trigger('click');            	
            }, 3000);
            //BA: 27/02/17 - return to home screen after 3 seconds - end
        }
    });
    
  
    //BA: 03/03/17 - turn of draggable - start
    $('img, .esc-icon').on('touchstart, taphold', function(e){
    	//console.log("tap hold or touchstart...");
    	$(this).preventDefault;
    });
    //BA: 03/03/17 - turn of draggable - end
    
    $('#books-toggle').on('click', function(){
    	$('#books-img').toggle();
    	$('#books-text').toggle();
    	if($(this).html() == 'View text'){
    		$(this).html('View image');
    	}
    	else{
    		$(this).html('View text');    		
    	}
    });
    
    //BA: 30/05/17 - disable soft keyboard on kiosk devices - start
    $('[type="text"], [type="email"], textarea').on('focus', function(){
    	if($(window).height() > 1500){
            //evt.preventDefault();
    		//$(this).trigger('blur');
    		//console.log("blur triggered");
    	}
    });    
    //BA: 30/05/17 - disable soft keyboard on kiosk devices - end
    
    //BA: 10/07/17 - try to fix keyboard popping up everytime icon pressed - start
    //$('.esc-icon, h3').on('focus, click', function(){
    //	$(this).trigger('blur');
    //});
    //BA: 10/07/17 - try to fix keyboard popping up everytime icon pressed - end
    
    //BA: Handle opening times accordion effect - start
    $('.opening-time-head').on('click', function(){
    	//console.log("clikck");
    	var isVisible = $(this).parent().parent().find('.opening-time-detail').is(':visible');
    	//var isVisible = $(this).find('.opening-time-detail').is(':visible');
    	$('.opening-time-detail').hide();
    	//var strId = '#' + $(this).attr('data-id');
    	//$(strId).toggle();
    	$(this).parent().parent().find('.opening-time-detail').toggle();
    	//$(this).find('.opening-time-detail').toggle();
    	if(isVisible){
        	$(this).parent().parent().find('.opening-time-detail').hide();    		
        	//$(this).find('.opening-time-detail').hide();    		
    	}
    	else{
        	$(this).parent().parent().find('.opening-time-detail').show();    		    		
        	//$(this).find('.opening-time-detail').show();    		    		
    	}
/*    	
    	if($(this).find('.symbol').html() == "+"){
    		$(this).find('.symbol').html('-');
    	}
    	else{
    		$(this).find('.symbol').html('+');
    	}
*/    	
    });
    //BA: Handle opening times accordion effect - end

    
    
    //BA: Handle opening times detail image click - start
    $('.detail-img, .map-link').on('click', function(){
    	strBack = "#esc-opening-times-link";
    	//var strMapSrc = "http://www.manchester.ac.uk/discover/maps/interactive-map/embed/";
    	//strUrl = 'http://www.manchester.ac.uk/discover/maps/interactive-map/embed/?libraries&id=' + $(this).attr('data-id');
    	strUrl = strMapSrc + '?libraries&id=' + $(this).attr('data-id');
    	try{
        	$('#esc-int-map-link').trigger('click');
        	$('#esc-map').attr('src', strUrl);
    	}
    	catch(e){
    		//ignore...
    	}    	
    });
    //BA: Handle opening times detail image click - end
    
    
    //BA: Track Library chat access with google analytics - start
    $('.uom_liveChat_tab').on('click', function(){
    	if($(this).parent().hasClass('hide')){
            ga('set', 'page', 'Library chat');
            ga('send', 'pageview');
    	}
    });
    //BA: Track Library chat access with goodle analytics - end
    
    //$(window).scroll(function(){
    	//$('header').css('position', 'fixed').css('top', '0');
    	//$('#esc-main-content').css('top', '180px');
    //})
    //$('img, .esc-icon').draggable('disable');  //BA: 03/03/17 - turn of draggable - start

    //BA: hide library chat on kiosks - start
    	if($(window).height() > 1500){
    		//console.log("HIDING!!!!!!!!!!");
			$('.uom_liveChat_container').hide();
			//$('.uom_liveChat_tab').hide();
    	}
     //BA: hide library chat on kiosks - end

/*    	
		if($(window).width() < 670){
			//alert("do css stuff here...");
		    //#esc-back, #esc-back img, #esc-help-img{
			$('#esc-help-img').css('display', 'none !important');
			$('#esc-back').css('display', 'none !important');
			$('#esc-back img').css('display', 'none !important');
		}
*/    	
    //console.log("viewport width: " + $(window).width() + ", viewport height: " + $(window).height());

    	
});// (document).ready end                                      

//BA: 11/07/17 - change font weight by device - start
$(window).load(function(){
	if(!isMobile){
		$('td, div, span').removeClass('mobile-device');
	}
	else{
		$('.ui-dialog-title').addClass('mobile-device');
		$('#feedback-div div').addClass('mobile-device');
		$('.esc-widget span').addClass('mobile-device');
		$('.largeTitle').addClass('mobile-device');
		//$('text tspan').addClass('mobile-device');
		//alert("width = " + $(window).width() + ", height = " + $(window).height());
	}
	if(isIos){
		$('.esc-floor-plan img').addClass('ios-floor-plan');
	}
});
//BA: 11/07/17 - change font weight by device - end



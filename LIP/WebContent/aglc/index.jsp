<%-- <%@page import="org.apache.log4j.Logger"%> --%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "x" uri = "http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<!--<![endif]--> 
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="text/html;IE=edge">
        <title>Library Information Point</title>
        <meta name="description" content="">
        
        <!-- enable device scaling -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=yes">
        
        <!-- BA: [14/11/14] NO CACHE - start -->
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
        <meta http-equiv="Pragma" content="no-cache"/>
        <meta http-equiv="Expires" content="-1"/>
        <!-- BA: [14/11/14] NO CACHE - end -->

        <link rel="SHORTCUT ICON" href="../resources/images/favicon.ico" />
                                
        <link type="text/css" rel="stylesheet" href="../resources/css/normalize.css">
        <link type="text/css" rel="stylesheet" href="../resources/css/main.css">



        <link type="text/css" rel="stylesheet" href="../resources/css/_boxes.css">
        <link type="text/css" rel="stylesheet" href="../resources/css/global.css">

        <!-- BA: Embed Open Sans Web Font - start -->
        <link type="text/css" rel="stylesheet" href="../resources/fonts/Open_Sans/styles.css">
        <!-- BA: Embed Open Sans Web Font - end -->

        <link type="text/css" href="../resources/js/jquery-ui-1.11.4/jquery-ui.min.css">
        <!-- <link type="text/css" href="../resources/js/jquery-ui-1.11.4/jquery-ui-1.10.4.custom.min.css"> -->

        <!-- BA: jquery mobile css -->
        <link type="text/css" rel="stylesheet" href="../resources/js/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.css">

        <style id="antiClickjack">body{display:none !important;}</style>        
        <script type="text/javascript" src="../resources/js/vendor/modernizr-2.6.2.min.js"></script>                                           

        <link type="text/css" rel="stylesheet" href="../resources/css/chat-widget.css">

        <script type="text/javascript" src="../resources/js/library-chat.js"></script>


        <!-- BA: timepicker resources - start -->
        <!-- <link rel="stylesheet" type="text/css" href="../resources/time-picker/jquery.timepicker.css"/> -->
        <!-- <link rel="stylesheet" type="text/css" href="../resources/time-picker/lib/bootstrap-datepicker.css"/> -->
        <!-- <link rel="stylesheet" type="text/css" href="../resources/time-picker/lib/site.css"/> -->
        <!-- BA: timepicker resources - end -->

        <script type="text/javascript" src="../resources/js/jquery-2.1.4.min.js"></script>
        
        <!-- BA: jquery mobile - start -->
        <!-- BA: Script below is to fix bug in Chrome... -->
        <script>
            $(document).bind('mobileinit',function(){
                $.mobile.changePage.defaults.changeHash = false;
                $.mobile.hashListeningEnabled = false;
                $.mobile.pushStateEnabled = false;
            });
        </script>       
        <script type="text/javascript" src="../resources/js/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.js"></script>
        <!-- BA: jquery mobile - end -->

        <script type="text/javascript" src="../resources/js/jquery-ui-1.11.4/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../resources/js/plugins.js"></script>
        <script type="text/javascript" src="../resources/js/main.js"></script>
        
        <!-- BA: Optional highcharts stuff start -->
        <!-- <script type="text/javascript" src="../resources/js/highcharts.js"></script> -->
        <script type="text/javascript" src="../resources/js/highcharts.src.js"></script>
        <script type="text/javascript" src="../resources/js/modules/map.js"></script>
        <script type="text/javascript" src="../resources/js/exporting.js"></script>
        <!-- BA: Optional highcharts stuff end -->
        
        <!-- <script type="text/javascript" src="../resources/js/js-webshim/dev/polyfiller.js"></script> -->
        <script src="../resources/js/date.js"></script>
        
        <!-- timepicker resources start -->
        <!-- <script type="text/javascript" src="../resources/time-picker/jquery.timepicker.js"></script> -->      
        <!-- <script type="text/javascript" src="../resources/time-picker/lib/bootstrap-datepicker.js"></script> -->       
        <!-- <script type="text/javascript" src="../resources/time-picker/lib/site.js"></script> -->
        <!-- timepicker resources end -->

        <!-- <script type="text/javascript" src="../resources/js/jquery-public-coreV2.1.0.1.min.js"></script> -->
        <script type="text/javascript" src="../resources/js/jquery-public-coreV2.master.js"></script>
        <!-- <link rel="stylesheet" type="text/css" href="../resources/css/lip-1.0.1.min.css" /> -->
        <link rel="stylesheet" type="text/css" href="../resources/css/lip-master.css" />
        <!-- <script src="../resources/js/lip-1.0.2.min.js"></script> -->
        <script src="../resources/js/lip-master.js"></script>

        <script src="../resources/js/global-plugins.js"></script>
        <script src="../resources/js/global-test.js"></script>
       
    </head>
    <body style="overflow:hidden;">
    <!-- <body style="background-color:#f9f9f9;"> -->
    <!-- <body style="background-color:rgba(249, 249, 249, 1)"> -->
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <header>
        <!-- <header style="position:fixed; top:0;"> -->
            <div id="inheader" user-scalable=0>
                <div style="display:table-row; width:auto">
                    <div style="display:table-cell; width:33%; text-aligh:left; padding-left:54px;">
                        <div id="esc-main-logo">
                            <!--  CM: set margins to conform to http://www.brand.manchester.ac.uk/visual-identity/logo/ -->
                            <img id="esc-main-logo-img" src="../resources/images/LTD_TUOM_2SPE_C.png" width="170" height="71" alt="University of Manchester logo" title="University of Manchester logo"> 
                            <!--  TODO: align rest of content to these margins, test on different display sizes -->
                            <!--  TODO: check for svg versions of icons -->
                        </div>
                    </div>
                    <div style="display:table-cell; width:33%; text-align:center;"> <!--  CM: removed padding for alignment -->
                        <div id="esc-top-nav">
                            <div class="esc-home-div">
                                <!-- <img class="esc-nav esc-home esc-home-img esc-logo" rel="esc-home" src="../resources/images/MB__home.png" /> -->
                                <!-- <img class="esc-nav esc-home esc-home-img esc-logo" rel="esc-home" src="../resources/images/home.png" width="128" height="128" /> -->
                                <img class="esc-nav esc-home esc-home-img esc-logo" rel="esc-home" src="../resources/images/Home.png" width="100" height="100" alt="Home" title="Home" style="position:relative; top:17px;" /> <!-- CM: 100px to avoid stretch -->                              
                                <!-- <span id="esc-back" style="margin-left:40px; display:none;"><img class="esc-nav esc-home esc-home-img esc-logo" rel="esc-home" src="../resources/images/back3.png" width="100" height="100" alt="Back" title="Back" style="position:relative; top:17px;" /></span> -->                              
                                <span id="esc-back" style="margin-left:40px; display:none;"><img class="esc-home-img" src="../resources/images/back3.png" width="100" height="100" alt="Back" title="Back" style="position:relative; top:17px;" /></span>                              
                            </div>      
                        </div>
                    </div>
 
                    <div style="display:table-cell; width:33%; text-align:right; padding-right:54px;">
                        <div style="margin-right:10px; margin-top:-2px;">
                            <!-- <img id="esc-help-img" class="esc-nav" rel="esc-help" src="../resources/images/AdobeStock_help4.jpg" width="80" height="80" alt="Home" title="Help" style="margin-top:0px;"> -->
                            <!-- <img id="esc-help-img" class="esc-nav" rel="esc-help" src="../resources/images/help2b.png" width="60" height="60" alt="Help" title="Help" style="margin-top:5px; margin-right:10px"> -->
                            <img id="esc-help-img" class="esc-nav" rel="esc-help" src="../resources/images/Help5.png" alt="Help" title="Help">                         
                        </div>
                        
                        <div style="margin-top:10px; height:30px;">
                            <!-- <img id="esc-contact-img" class="esc-nav esc-logo" rel="esc-contact" src="../resources/images/Contact6.png" alt="Feedback" title="Feedback"> -->
                        </div>
                        
                    </div>
 
                </div>
            </div>
        </header>
                
        <div style="margin-top: 0em; background-color: #FFCC33; padding:5px; text-shadow:none; text-align:center;" class="mobile-device">
        		We would love to have your feedback on this app; please take a minute to complete our <a id="esc-questionnaire-link" rel="esc-questionnaire" href="javascript:this.preventDefault;">online questionnaire</a>.
      	</div>

		<div class="uom_liveChat_container hide">
		  <div class="uom_liveChat_tab">
		    <p>Can we help?</p>
		  </div>
		  
		  <div class="uom_liveChat_details">
		    <div id="live-chat" class="chat-widget"> 
		      <script language="javascript"  type="text/javascript">{ showChatWidget(); }</script> 
		    </div>
		  </div>
		</div>
		
            <!-- List of pages and their corresponding widgets - start -->            
            <div id="esc-home" class="esc-page">
<!--                 
                <div style="margin-top: -0.8em; margin-bottom: 1.5em;">
                    <span style="color: #660099; font-size: 1.3em;">Welcome to the <strong>Library Information Point.</strong> What would you like to do today?</span>
                </div>
 -->                
                <!-- Main Library Home Menu - start -->
<!--                 
                <div class="ui-form-row esc-top-row">
                        <div class="ui-form-row">
	                        <div id="esc-lib-search-link" rel="esc-lib-search" class="esc-icon esc-4col-left" title="Library Search">
	                            <img src="../resources/images/search2.png" />
	                            <img src="../resources/images/LibrarySearch.png" alt="Library Search" />                       
	                            <div><h3>Library Search</h3></div>
	                        </div>

                            <div id="esc-int-map-link" rel="esc-int-map" class="esc-icon esc-4col-middle esc-logo" title="View interactive campus map">
                                <img src="../resources/images/map.png" />
                                <img src="../resources/images/CampusMap.png" alt="View interactive campus map" />                          
                                <div><h3>View interactive campus map</h3></div>
                            </div>
                            
                            <div id="esc-pc-all-cluster-link" rel="esc-pc-all-cluster" class="esc-icon esc-4col-middle esc-logo esc-pc-cluster-link" title="View PC availability">
                                <img src="../resources/images/PC Cluster.png" alt="View PC Availability" />
                                <div><h3>View PC availability</h3></div>
                            </div>
                
                            <div id="esc-subject-guide-link" rel="esc-subject-guide" class="esc-icon esc-4col-right esc-logo" title="View subject locations">
                                <img src="../resources/images/guide.png" />
                                <img src="../resources/images/Subject guide.png" alt="View Subject Locations" />                       
                                <div><h3>View Subject Locations</h3></div>
                            </div>
                        </div>                              
                </div>
                
                <div class="ui-form-row esc-middle-row">
                    <div class="ui-form-row">

                        <div id="esc-plans-link" rel="esc-plans" class="esc-icon esc-4col-left esc-logo" style="margin-top: 1em;" title="View floor plans">
                            <img src="../resources/images/FloorPlans.png" alt="View floor plans" />                        
                            <div><h3>View floor plans</h3></div>
                        </div>

                        <div id="esc-find-book-link" rel="esc-find-book" class="esc-icon esc-4col-middle esc-logo" title="How to find a book">
                            <img src="../resources/images/FindBook.png" alt="Locating books on the shelf" />                        
                            <div><h3>Locating books on the shelf</h3></div>
                        </div>
     
                        <div id="esc-opening-times-link" rel="esc-opening-times" class="esc-icon esc-4col-middle esc-logo" title="View opening times">
                            <img src="../resources/images/Opening Times.png" alt="View Opening Times" />
                            <div><h3>View Opening Times</h3></div>
                        </div>
                        
                        <div id="esc-contact-link" rel="esc-contact" class="esc-icon esc-4col-right esc-logo" style="margin-top: 1em;" title="Feedback">
                            <img src="../resources/images/Feedback.png" alt="Feedback" />                        
                            <div><h3>Feedback</h3></div>
                        </div>
                            
                    </div>                              
                </div>
 -->                
                <!-- Main Library Home Menu - end -->

                <!-- AGLC Home Menu - start -->
                 
                <div class="ui-form-row esc-top-row">
                        <div class="ui-form-row">
                            <div id="esc-int-map-link" rel="esc-int-map" class="esc-icon esc-4col-left esc-logo" title="View interactive campus map">
                                <img src="../resources/images/CampusMap.png" alt="View interactive campus map" />                          
                                <div><h3>View interactive campus map</h3></div>
                            </div>
                            
                            <div rel="esc-pc-all-cluster" class="esc-icon esc-4col-middle esc-logo esc-pc-cluster-link" title="View PC availability">
                                <img src="../resources/images/PC Cluster.png" alt="View PC Availability" />
                                <div><h3>View PC availability</h3></div>
                            </div>
                
	                        <div id="esc-opening-times-link" rel="esc-opening-times" class="esc-icon esc-4col-middle esc-logo" title="View opening times">
	                            <img src="../resources/images/Opening Times.png" alt="View Opening Times" />
	                            <div><h3>View Opening Times</h3></div>
	                        </div>
                            
	                        <!-- <div id="esc-contact-link" rel="esc-contact" class="esc-icon esc-4col-right esc-logo" style="margin-top: 1em;" title="Feedback"> -->
	                        <div id="esc-questionnaire-link" rel="esc-questionnaire" class="esc-icon esc-4col-right esc-logo" style="margin-top: 1em;" title="Feedback">
	                            <img src="../resources/images/Feedback.png" alt="Feedback" />                        
	                            <div><h3>Feedback</h3></div>
	                        </div>                            
                            
                        </div>                              
                </div>
<!--                 
                <div class="ui-form-row esc-middle-row">
                    <div class="ui-form-row">
                        <div id="esc-contact-link" rel="esc-contact" class="esc-icon esc-4col-left esc-logo" style="margin-top: 1em;" title="Ask a question">
                            <img src="../resources/images/Feedback.png" alt="Ask a question" />                        
                            <div><h3>Feedback</h3></div>
                        </div>
                        
                        <div id="" rel="" class="esc-icon esc-4col-middle esc-logo" title="">
							&nbsp;
                        </div>
        
                        <div id="" rel="" class="esc-icon esc-4col-middle esc-logo" style="margin-top: 1em;" title="">
                        </div>
    
                        <div id="" rel="" class="esc-icon esc-4col-right esc-logo" title="">
                        </div>
                                                    
                    </div>                              
                </div>                               
 -->                
                <!-- AGLC Home Menu - end -->
                                
            </div>
            
            <!-- List of pages and their corresponding widgets - end -->
            
            <!-- List of widget contents - start -->
            <div style="overflow:auto;"> 
            	<div id="esc-questionnaire" style="display:none;">
	                <div style="margin-bottom: 0.5em; text-align:center;">
	                    <span style="color: #660099; font-size: 1.3em;"><strong>Feedback questionnaire</strong></span>
	                </div>
            		<!-- <iframe id="esc-quaire" contenteditable="true" src="https://manchesterlib.eu.qualtrics.com/SE/?SID=SV_55uaszXwYu6dvMx"></iframe> -->
            		<!-- <iframe id="esc-quaire" contenteditable="true" src="https://manchesterlib.eu.qualtrics.com/jfe/form/SV_cGdhvw0LmBQa6BT"></iframe> -->
            		<iframe id="esc-quaire" contenteditable="true" src=""></iframe>
            	</div>
                      
                <div id="esc-int-map" class="esc-widget" style="display:none;">
	                <div style="margin-bottom: 0.5em; text-align:center;">
	                    <span style="color: #660099; font-size: 1.3em;"><strong>Interactive map</strong></span>
	                </div>
                    <!-- <iframe id="esc-map" contenteditable="true" src="http://www.manchester.ac.uk/discover/maps/interactive-map/embed/"></iframe> -->
                    <iframe id="esc-map" contenteditable="true" src=""></iframe>                    
                </div>

                <div id="esc-room-booking" class="esc-widget" style="display:none; overflow:hidden;">             
	                <div style="margin-top: -0.4em; margin-bottom: 0.5em; text-align:center;">
	                    <span style="color: #660099; font-size: 1.3em;"><strong>Room booking</strong></span>
	                </div>
                    <div>Put room booking stuff here...
						<%-- <c:import var = "openXML" url = "http://systems.library.manchester.ac.uk/blackboard_webservices/places?tag=10"/> --%>                    
                    	<%--<c:import var = "auth" url = "http://130.88.145.66:8194/RBREST/Auth?UserName=User&Password=SBElectronicSystemsLtd"/>--%> 
                    	<%-- <c:import var = "auth" url = "http://User:SBElectronicSystemsLtd@130.88.145.66:8194/RBREST/Auth"/> --%> 
                    	<%-- <c:import var = "auth" url = "http://130.88.145.66:8194/RBREST/Auth?UserName=User&Password=SBElectronicSystemsLtd" /> --%> 
                    	<%-- <c:import var = "groupsJson" url="http://130.88.145.66:8194/RBREST/Rooms/Groups"></c:import> --%>
                    	<%-- <c:out value="'Groups = ' ${groupsJson}"/> --%>                   
                    </div>                    
                </div>
                
                                
                <div style="display:table; width:100%; overflow:hidden;">           
                    <div style="display:table-row; width:100%;">
                        <div style="display:table-cell;">
                            <div id="esc-lib-search" class="esc-widget" style="display:none;">
				                <div style="margin-bottom: 0.5em; text-align:center;">
				                    <span style="color: #660099; font-size: 1.3em;"><strong>Library search</strong></span>
				                </div>
                            	<!-- BA: Using src="librarySearch/index.html#" makes page scroll to top but prevents resetting page content so leaving as is for now... -->
                                <!-- <iframe id="esc-search" src="librarySearch/index.html" contenteditable="true"></iframe> -->
                                <!-- <iframe id="esc-search" sandbox="allow-scripts allow-forms allow-pointer-lock allow-same-origin" src="https://man-fe.hosted.exlibrisgroup.com/primo-explore/search?vid=MU_VU1_BETA&lang=en_US&sortby=rank" contenteditable="true"></iframe> -->                                
                                <!-- <iframe id="esc-search" sandbox="allow-scripts allow-forms allow-pointer-lock allow-same-origin" src="https://man-fe.hosted.exlibrisgroup.com/primo-explore/search?vid=MU_VU1_BETA&lang=en_US&sortby=rank" contenteditable="true"></iframe> -->                                
                                <iframe id="esc-search" sandbox="allow-scripts allow-forms allow-pointer-lock allow-same-origin" src="" contenteditable="true"></iframe>                                                               
                            </div>
                        </div>
                    </div>
                </div>                                    
                                                           
				<div id="esc-find-book" class="esc-widget" style="display:none;">               
	                <div style="margin-top: -0.1em; margin-bottom: 0.5em; text-align:center;">
	                    <span style="color: #660099; font-size: 1.3em;"><strong>Locating books on the shelf</strong></span>
	                </div>

                    <div id="books-text2" style="overflow:hidden; display:block;" class="mainContentContainer">
                        <div style="overflow:auto; max-height:calc(100vh - 220px);">
							<div style="display: table-row">
	                            <div style="display:table-cell; width:1%;"></div>
	                            <div id="esc-find2" style="display:table-cell; width:98%; background-color:white; padding:10px; text-align:left;">
	                            
									<p>The Library uses the Dewey Decimal Classification scheme (Dewey for short) to arrange books and other resources on the shelves so you can locate them easily. We're going to show you how to use Dewey to find a book at any Library site.</p>
									<p>Once you've searched for your item on Library Search, you'll see something like this:</p>
									<p><img src="../resources/images/book-example.jpg" alt="Search result for a book" style="width: 632px; height: 212px;" class=""></p>
									<p>Note how it reads: "Available at Main Library Blue Area Floor 1 <strong>(370.193 B174)</strong>". The part in brackets is the bit we're interested in.</p><a id="d.en.573230"></a>
									
									<h2>What do these numbers next to the book's location mean?</h2>
									<p>The numbers shown below are a "shelfmark" (on Library Search, this is the number in brackets next to the book's location). You'll find one of these on every book in the Library - a lot of customers find this confusing at first but if you break down the information into three sections, it's easier to understand.</p>
									<p>&nbsp;<img src="../resources/images/FAB-web-example.jpg" alt="A shelfmark" style="width: 600px; height: 171px;" class=""></p>
																		
									<ol>
									<li>The numbers in front of the decimal point indicate the subject area (in this case 370). Look for this number first.</li>
									<li>Next, look for the number after the decimal point (in this case 193). <strong>You should do this is digit-by-digit.</strong> <br>In this example, look first for .1 (which comes before .2, .3, .4...) then for .19 (which comes before .20, .21, .22...) and then .193 (which comes before .194, .195, .196...).</li>
									<li>Now look for the letter(s) and number(s). Letters are shelved alphabetically, numbers are shelved in order. Letters are usually the initial letter(s) of the author's name (in this case, Burbules).</li>
									</ol>									
									
									<h2>Shelves</h2>
									<ul>
									<li>Look for the signs at the end of each row: they show the range of shelfmarks on those shelves.</li>
									<li>Rows run from left-to-right, and books are shelved from left-to-right.</li>
									</ul>
									<h2>Collections</h2>
									<ul>
									<li>Sometimes, you will see extra letters before the shelfmark. This shows that a book is part of a collection within the Library. For example: N.E. 279.32/R29 is part of the Near Eastern collection.</li>
									<li>Find the collection first, before looking for the number.</li>
									</ul>
									<h2>Ask for help</h2>
									<ul>
									<li>Most importantly, if you are having difficulty locating a book, or have any other questions please just speak to a member of staff who will be happy to assist you.</li>
									</ul>	                            
	                            
	                            </div>
	                            <div style="display:table-cell; width:1%;"></div>
                            </div>
                        </div>
	                </div>        				                    
                </div>
                

				<div id="esc-contact" class="esc-widget" style="display:none; padding-top:10px; padding-bottom:10px;">               
	                <div style="margin-bottom: 0.5em; text-align:center;">
	                    <span style="color: #660099; font-size: 1.3em;"><strong>Customer feedback</strong></span>
	                </div>
                    <div style="margin-left:10px; margin-right:10px;">                        
                        <div style="overflow:auto;  max-height:calc(100vh - 300px);">
							<div style="display:table; width:100%;">
		                        <div style="display: table-row;">
		                            <div style="display:table-cell; width:5%;"></div>
		                            <div id="esc-feedback" style="display:table-cell; width:90%; overflow:auto;">
		                                <div>
		                                    <h1>Customer feedback</h1>
		                                    <p class="subHeading">We welcome feedback from all our customers.</p>
		                                    <p>We will reply to you within five working days of receipt of your message.</p>
		                                </div>
		                                <form id="esc-feedback-form" name="Feedback">
		                                    <div style="border-style:solid; border-width:1px; border-color:#CCCCCC; padding:10px;">
		                                        <div id="esc-notes">
		                                            <h2> Contact information</h2>
		                                            <p>Please enter your full email address, for example, name@domain.com</p>
		                                            <p>It is important that you provide a valid, working email address that you have legitimate access to.</p>
		                                            <p>Your information is protected by our <a href="http://www.manchester.ac.uk/privacy/" target="_blank" title="View our Privacy Policy">Privacy Policy</a></p>
		                                        </div>
		                                        <div>
		                                            <label for="esc-name">Name*</label>
		                                        </div>
		                                        <div>
		                                            <input id="esc-name" type="text" required="required" placeholder="Your name" maxlength="50" name="name" size="25" class="esc-form-field" />                             
		                                        </div>
		                                        <p>
		                                        <div>
		                                            <label for="esc-email">Email address*</label>
		                                        </div>
		            
		                                        <div>
		                                            <input id="esc-email" type="email" name="email" required="required" size="40" maxlength="50" placeholder="your.name@yourhost.com" class="esc-form-field" />
		                                        </div>
		                                    </div>
		                                    <p>
		                                    <div style="border-style:solid; border-width:1px; border-color:#CCCCCC; padding:10px;">
		                                        <h2>Your comments</h2>
		                                        <div>
		                                            <label for="esc-comments">Comments*</label>
		                                        </div>
		                                        <div>
		                                            <textarea id="esc-comments" rows="10" cols="45" name="comments" required wrap="hard" placeholder="Please enter your feedback here." class="esc-form-field"></textarea>
		                                        </div>
		                                    </div>
		                                    <p>
		                                    <div>
		                                        <input type="submit" id="esc-submit" name="submit" value="Submit" />
		                                        <input type="button" value="Reset" id="esc-reset" />
		                                        <!-- <input type="button" id="esc-fill" value="Autofill Form" /> -->
		                                    </div>                      
		                                </form>
		                            </div>
		                            <div style="display:table-cell; width:5%;"></div>                      
		                        </div>
	                    	</div>                        
                        </div>
                    </div>
                </div>

                <div id="esc-subject-guide" class="esc-widget" style="display:none;">               
	                <div style="margin-bottom: 0.5em; text-align:center;">
	                    <span style="color: #660099; font-size: 1.3em;"><strong>Subject guide</strong></span>
	                </div>
                    <div style="margin-left:10px; margin-right:10px;">
                        <div style="width:auto; display:inline-block; margin-bottom:10px;">
                            <div style="display:inline-block">
                                <div id="esc-all" style="display:inline-block">All</div>
                                <div class="esc-a-z" id="esc-Aa" style="display:inline-block">Aa</div>
                                <div class="esc-a-z" id="esc-Bb" style="display:inline-block">Bb</div>
                                <div class="esc-a-z" id="esc-Cc" style="display:inline-block">Cc</div>
                                <div class="esc-a-z" id="esc-Dd" style="display:inline-block">Dd</div>
                                <div class="esc-a-z" id="esc-Ee" style="display:inline-block">Ee</div>
                                <div class="esc-a-z" id="esc-Ff" style="display:inline-block">Ff</div>
                                <div class="esc-a-z" id="esc-Gg" style="display:inline-block">Gg</div>
                                <div class="esc-a-z" id="esc-Hh" style="display:inline-block">Hh</div>
                                <div class="esc-a-z" id="esc-Ii" style="display:inline-block">Ii</div>
                                <div class="esc-a-z" id="esc-Jj" style="display:inline-block">Jj</div>
                                <div class="esc-a-z" id="esc-Kk" style="display:inline-block">Kk</div>
                                <div class="esc-a-z" id="esc-Ll" style="display:inline-block">Ll</div>
                                <div class="esc-a-z" id="esc-Mm" style="display:inline-block">Mm</div>
                                <div class="esc-a-z" id="esc-Nn" style="display:inline-block">Nn</div>
                                <div class="esc-a-z" id="esc-Oo" style="display:inline-block">Oo</div>
                                <div class="esc-a-z" id="esc-Pp" style="display:inline-block">Pp</div>
                                <div class="esc-a-z" id="esc-Qq" style="display:inline-block">Qq</div>
                                <div class="esc-a-z" id="esc-Rr" style="display:inline-block">Rr</div>
                                <div class="esc-a-z" id="esc-Ss" style="display:inline-block">Ss</div>
                                <div class="esc-a-z" id="esc-Tt" style="display:inline-block">Tt</div>
                                <div class="esc-a-z" id="esc-Uu" style="display:inline-block">Uu</div>
                                <div class="esc-a-z" id="esc-Vv" style="display:inline-block">Vv</div>
                                <div class="esc-a-z" id="esc-Ww" style="display:inline-block">Ww</div>
                                <div class="esc-a-z" id="esc-Xx" style="display:inline-block">Xx</div>
                                <div class="esc-a-z" id="esc-Yy" style="display:inline-block">Yy</div>
                                <div class="esc-a-z" id="esc-Zz" style="display:inline-block">Zz</div>
                            </div>
                        </div>
                        <div style="overflow:auto;  max-height:calc(100vh - 280px);" id="subLoc"> <!--  CM: Added ID for dynamic updating -->
                        <table style="border-style:solid; margin-top:10px;" id="subLocData"> <!--  CM: Added ID for dynamic updating -->
                            <thead>
                                <tr>
                                    <th>Subject</th>
                                    <th>Classification</th>
                                    <th>Book Location</th>
                                </tr>
                            </thead>
                            <!--  CM: this will be populated dynamically -->
                            <tbody id="subLocBody">
                                <tr class="Aa esc-guide">
                                    <td class="mobile-device">Agriculture</td>
                                    <td class="mobile-device">630 - 639</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>
                                <tr class="Aa esc-guide">
                                    <td class="mobile-device">Alacran Collection</td>
                                    <td class="mobile-device">Bay M</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>
                                <tr class="Aa esc-guide">
                                    <td class="mobile-device">American Literature</td>
                                    <td class="mobile-device">810 - 819</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>
                                <tr class="Aa esc-guide">
                                    <td class="mobile-device">Ancient History</td>
                                    <td class="mobile-device">930 - 939</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>
                                <tr id="Aa" class="Aa esc-guide">
                                    <td class="mobile-device">Anthropology</td>
                                    <td class="mobile-device">306</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>
                                <tr class="Aa esc-guide">
                                    <td class="mobile-device">Arabic Language and Literature</td>
                                    <td class="mobile-device">Ne Collection</td>
                                    <td><a href="#" title="Orange 4" rel="orange4" class="esc-floor-link">(Orange and Purple 4 and Orange 5)</a></td>
                                </tr>
                                <tr class="Aa esc-guide">
                                    <td class="mobile-device">Archaeology</td>
                                    <td class="mobile-device">913</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>
                                <tr class="Aa esc-guide">
                                    <td class="mobile-device">Architecture</td>
                                    <td class="mobile-device">720 - 729</td>
                                    <td><a href="#" title="Blue 4" rel="blue4" class="esc-floor-link">Blue 4 (&amp; Kantorowich)</a></td>
                                </tr>
                                <tr class="Aa esc-guide">
                                    <td class="mobile-device">Art</td>
                                    <td class="mobile-device">700 - 709, 730 - 779</td>
                                    <td><a href="#" title="Blue 4" rel="blue4" class="esc-floor-link">Blue 4</a></td>
                                </tr>
                                <tr class="Aa esc-guide">
                                    <td class="mobile-device">Astronomy</td>
                                    <td class="mobile-device">520 - 529</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>
                                <tr class="Bb esc-guide">
                                    <td class="mobile-device">Biology</td>
                                    <td class="mobile-device">570 - 579</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>
                                <tr class="Bb esc-guide">
                                    <td class="mobile-device">Botany</td>
                                    <td class="mobile-device">580 - 589</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>
                                <tr class="Bb esc-guide">
                                    <td>Building</td>
                                    <td>690 - 699</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2 (&amp; Joule)</a></td>
                                </tr>
                                <tr class="Bb esc-guide">
                                    <td>Building Conservation</td>
                                    <td>711.025</td>
                                    <td><a href="#" title="Blue 4" rel="blue4" class="esc-floor-link">Blue 4</a></td>
                                </tr>
                                <tr class="Bb esc-guide">
                                    <td>Business Studies</td>
                                    <td>338.93, 650 - 654, 656 - 659</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2 (&amp; Eddie Davies &amp; Precinct)</a></td>
                                </tr>
                                <tr class="Cc esc-guide">
                                    <td>Celtic Languages</td>
                                    <td>491.6</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                               
                                <tr class="Cc esc-guide">
                                    <td>Celtic Literature</td>
                                    <td>891.6</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                               
                                <tr class="Cc esc-guide">
                                    <td>Chemical Technology</td>
                                    <td>660 - 668</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2 (&amp; Joule)</a></td>
                                </tr>                               
                                <tr class="Cc esc-guide">
                                    <td>Chemistry</td>
                                    <td>540 - 547</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                               
                                <tr class="Cc esc-guide">
                                    <td>Cinema &amp; Television</td>
                                    <td>791.4 - 791.44</td>
                                    <td><a href="#" title="Blue 4" rel="blue4" class="esc-floor-link">Blue 4</a></td>
                                </tr>                               
                                <tr class="Cc esc-guide">
                                    <td>Commerce and Communications</td>
                                    <td>380 - 389</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2 (&amp; Eddie Davies &amp; Precinct)</a></td>
                                </tr>                               
                                <tr class="Cc esc-guide">
                                    <td>Comparative Language</td>
                                    <td>400 - 416, 418 - 419</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                               
                                <tr class="Cc esc-guide">
                                    <td>Comparative Literature</td>
                                    <td>800 - 809</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                               
                                <tr class="Cc esc-guide">
                                    <td>Computer Science</td>
                                    <td>003 - 006</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2 (&amp; Joule)</a></td>
                                </tr>                               
                                <tr class="Cc esc-guide">
                                    <td>Counselling</td>
                                    <td>Varied (see Catalogue)</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                               
                                <tr class="Cc esc-guide">
                                    <td>Crystallography</td>
                                    <td>548 - 549</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                               
                                <tr class="Cc esc-guide">
                                    <td>Customs &amp; Folklore</td>
                                    <td>390 - 399</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                                                               
                                <tr class="Dd esc-guide">
                                    <td>Dentistry</td>
                                    <td>617.6 - 617.99</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                               
                                <tr class="Dd esc-guide">
                                    <td>Domestic Economy</td>
                                    <td>640 - 649</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                               
                                <tr class="Dd esc-guide">
                                    <td>Drama</td>
                                    <td>792</td>
                                    <td><a href="#" title="Blue 4" rel="blue4" class="esc-floor-link">Blue 4</a></td>
                                </tr>                               
                                <tr class="Ee esc-guide">
                                    <td>East Asian</td>
                                    <td>East Asian Collection</td>
                                    <td><a href="#" title="Purple 2" rel="purple2" class="esc-floor-link">Purple 2</a></td>
                                </tr>                               
                                <tr class="Ee esc-guide">
                                    <td>Economics</td>
                                    <td>330 - 339</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                               
                                <tr class="Ee esc-guide">
                                    <td>Economic History</td>
                                    <td>309</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                               
                                <tr class="Ee esc-guide">
                                    <td>Education</td>
                                    <td>370 - 379</td>
                                    <td><a href="#" title="Blue 1" rel="blue1" class="esc-floor-link">Blue 1</a></td>
                                </tr>                               
                                <tr class="Ee esc-guide">
                                    <td>Electrical Engineering</td>
                                    <td>621.3</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2 (&amp; Joule)</a></td>
                                </tr>                               
                                <tr class="Ee esc-guide">
                                    <td>Engineering</td>
                                    <td>620 - 629</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2 (&amp; Joule)</a></td>
                                </tr>                               
                                <tr class="Ee esc-guide">
                                    <td>English Language</td>
                                    <td>420 - 429</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                               
                                <tr class="Ee esc-guide">
                                    <td>English Literature</td>
                                    <td>820 - 829</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                               
                                <tr class="Ee esc-guide">
                                    <td>Ethics &amp; Logic</td>
                                    <td>170 - 179</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                               
                                <tr class="Ff esc-guide">
                                    <td>French Language</td>
                                    <td>440 - 448</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                               
                                <tr class="Ff esc-guide">
                                    <td>French Literature</td>
                                    <td>840 - 848</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                               
                                <tr class="Gg esc-guide">
                                    <td>General Science</td>
                                    <td>500 - 509</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                               
                                <tr class="Gg esc-guide">
                                    <td>Geography</td>
                                    <td>910 - 912, 914 - 919</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                               
                                <tr class="Gg esc-guide">
                                    <td>Geology</td>
                                    <td>550 - 559</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                               
                                <tr class="Gg esc-guide">
                                    <td>German Language</td>
                                    <td>430 - 439</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                               
                                <tr class="Gg esc-guide">
                                    <td>German Literature</td>
                                    <td>830 - 839</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                               
                                <tr class="Gg esc-guide">
                                    <td>Greek Language</td>
                                    <td>480 - 489</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                               
                                <tr class="Gg esc-guide">
                                    <td>Greek Literature</td>
                                    <td>880 - 889</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                               
                                <tr class="Hh esc-guide">
                                    <td>Health Policy/Social Policy</td>
                                    <td>360 - 369</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                               
                                <tr class="Hh esc-guide">
                                    <td>History - General</td>
                                    <td>900 - 909</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                               
                                <tr class="Hh esc-guide">
                                    <td>History - Medieval &amp; Modern</td>
                                    <td>940 - 952, 954, 957 - 999</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                               
                                <tr class="Hh esc-guide">
                                    <td>Human Communication</td>
                                    <td>004 - 808</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Bays H and J (Blue 2) Closed Collection. Since 2007, interfiled with Medical 610 - 619</a></td>
                                </tr>                               
                                <tr class="Ii esc-guide">
                                    <td>International Development/IDPM</td>
                                    <td></td>
                                    <td><a href="#" title="Muriel Stott" rel="murielStott" class="esc-floor-link">Muriel Stott (Through walkway off Blue Ground)</a></td>
                                </tr>                               
                                <tr class="Ii esc-guide">
                                    <td>International Law</td>
                                    <td>341</td>
                                    <td><a href="#" title="Green 3" rel="green3" class="esc-floor-link">Green 3</a></td>
                                </tr>                               
                                <tr class="Ii esc-guide">
                                    <td>Italian Language</td>
                                    <td>450 - 458</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                               
                                <tr class="Ii esc-guide">
                                    <td>Italian Literature</td>
                                    <td>850 - 859</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                               
                                <tr class="Jj esc-guide">
                                    <td>J</td>
                                    <td colspan="2">No items listed</td>
                                </tr>                               
                                <tr class="Kk esc-guide">
                                    <td>K</td>
                                    <td colspan="2">No items listed</td>
                                </tr>                               
                                <tr class="Ll esc-guide">
                                    <td>Law</td>
                                    <td>340, 342 - 349</td>
                                    <td><a href="#" title="Green 3" rel="green3" class="esc-floor-link">Green 3</a></td>
                                </tr>                               
                                <tr class="Ll esc-guide">
                                    <td>Logic and Ethics</td>
                                    <td>160 - 179</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                               
                                <tr class="Mm esc-guide">
                                    <td>Maps</td>
                                    <td></td>
                                    <td><a href="#" title="Purple Ground" rel="purpleG" class="esc-floor-link">Purple Ground</a></td>
                                </tr>                               
                                <tr class="Mm esc-guide">
                                    <td>Mathematics</td>
                                    <td>510 - 519</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                               
                                <tr class="Mm esc-guide">
                                    <td>Medical and Human Sciences</td>
                                    <td>610 - 619</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                               
                                <tr class="Mm esc-guide">
                                    <td>Metallurgy</td>
                                    <td>669</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2 (&amp; Joule)</a></td>
                                </tr>                               
                                <tr class="Mm esc-guide">
                                    <td>Military Studies</td>
                                    <td>355 - 359</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                               
                                <tr class="Mm esc-guide">
                                    <td>Mineralogy</td>
                                    <td>549</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                               
                                <tr class="Mm esc-guide">
                                    <td>Museum Studies</td>
                                    <td>355 - 359</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                               
                                <tr class="Mm esc-guide">
                                    <td>Music</td>
                                    <td></td>
                                    <td><a href="#" title="Red 2" rel="red2" class="esc-floor-link">Red 2</a></td>
                                </tr>
                                <tr class="Nn esc-guide">
                                    <td>Nursing</td>
                                    <td>610 - 619</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>
                                <tr class="Oo esc-guide">
                                    <td>Oriental Languages/East Asian Collection</td>
                                    <td></td>
                                    <td><a href="#" title="Purple 2" rel="putple2" class="esc-floor-link">Purple 2</a></td>
                                </tr>
                                <tr class="Pp esc-guide">
                                    <td>Painting - General, inc Fine Art</td>
                                    <td>750 - 752</td>
                                    <td><a href="#" title="Blue 4" rel="blue4" class="esc-floor-link">Blue 4</a></td>
                                </tr>                               
                                <tr class="Pp esc-guide">
                                    <td>Painting: Individual Artists</td>
                                    <td>750.92</td>
                                    <td><a href="#" title="Blue 4" rel="blue4" class="esc-floor-link">Blue 4 (&amp; Kantorowich)</a></td>
                                </tr>                               
                                <tr class="Pp esc-guide">
                                    <td>Painting - Portraits</td>
                                    <td>757</td>
                                    <td><a href="#" title="Blue 4" rel="blue4" class="esc-floor-link">Blue 4</a></td>
                                </tr>                               
                                <tr class="Pp esc-guide">
                                    <td>Painting: subdivided geographically by area/country</td>
                                    <td>750.94 - 750.99</td>
                                    <td><a href="#" title="Blue 4" rel="blue4" class="esc-floor-link">Blue 4</a></td>
                                </tr>
                                <tr class="Pp esc-guide">
                                    <td>Palaeonology</td>
                                    <td>650 - 669</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                                                                                               
                                <tr class="Pp esc-guide">
                                    <td>Pharmacy</td>
                                    <td>615 - 615.99</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                                                                                               
                                <tr class="Pp esc-guide">
                                    <td>Philosophy</td>
                                    <td>100 - 129, 140 - 149</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                                                                                               
                                <tr class="Pp esc-guide">
                                    <td>Photography</td>
                                    <td>770 - 779</td>
                                    <td><a href="#" title="Blue 4" rel="blue4" class="esc-floor-link">Blue 4</a></td>
                                </tr>                                                                                               
                                <tr class="Pp esc-guide">
                                    <td>Physics</td>
                                    <td>530 - 539</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                                                                                               
                                <tr class="Pp esc-guide">
                                    <td>Planning</td>
                                    <td>711 - 712</td>
                                    <td><a href="#" title="Blue 4" rel="blue4" class="esc-floor-link">Blue 4 (&amp; Kantorowich)</a></td>
                                </tr>                                                                                               
                                <tr class="Pp esc-guide">
                                    <td>Political Science</td>
                                    <td>320 - 329</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                                                                                               
                                <tr class="Pp esc-guide">
                                    <td>Portuguese Language</td>
                                    <td>469</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                                                                                               
                                <tr class="Pp esc-guide">
                                    <td>Portuguese Literature</td>
                                    <td>869</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                                                                                               
                                <tr class="Pp esc-guide">
                                    <td>Psychology</td>
                                    <td>130 - 159</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                                                                                               
                                <tr class="Pp esc-guide">
                                    <td>Public Administrations</td>
                                    <td>350 - 359</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                                                                                               
                                <tr class="Qq esc-guide">
                                    <td>Q</td>
                                    <td colspan="2">No items listed</td>
                                </tr>                               
                                <tr class="Rr esc-guide">
                                    <td>Recreations</td>
                                    <td>790 - 799</td>
                                    <td><a href="#" title="Blue 4" rel="blue4" class="esc-floor-link">Blue 4</a></td>
                                </tr>                                                                                               
                                <tr class="Rr esc-guide">
                                    <td>Religion/Theology</td>
                                    <td>200 - 299</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                                                                                               
                                <tr class="Rr esc-guide">
                                    <td>Romance Languages</td>
                                    <td>479.5</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                                                                                               
                                <tr class="Rr esc-guide">
                                    <td>Romance Literature</td>
                                    <td>879.5</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                                                                                               
                                <tr class="Rr esc-guide">
                                    <td>Russian Language</td>
                                    <td>491.7</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                                                                                               
                                <tr class="Rr esc-guide">
                                    <td>Russian Literature</td>
                                    <td>891.7</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                                                                                               
                                <tr class="Ss esc-guide">
                                    <td>Screenwriting</td>
                                    <td>791.44</td>
                                    <td><a href="#" title="Blue 4" rel="blue4" class="esc-floor-link">Blue 4</a></td>
                                </tr>                                                                                               
                                <tr class="Ss esc-guide">
                                    <td>Social Policy/Health Policy</td>
                                    <td>360 - 369</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                                                                                               
                                <tr class="Ss esc-guide">
                                    <td>Sociology</td>
                                    <td>300 - 308</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                                                                                               
                                <tr class="Ss esc-guide">
                                    <td>Spanish Language</td>
                                    <td>460 -468</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                                                                                               
                                <tr class="Ss esc-guide">
                                    <td>Spanish Literature</td>
                                    <td>860 -869</td>
                                    <td><a href="#" title="Blue 3" rel="blue3" class="esc-floor-link">Blue 3</a></td>
                                </tr>                                                                                               
                                <tr class="Ss esc-guide">
                                    <td>Sports</td>
                                    <td>796 - 799</td>
                                    <td><a href="#" title="Blue 4" rel="blue4" class="esc-floor-link">Blue 4</a></td>
                                </tr>                                                                                               
                                <tr class="Ss esc-guide">
                                    <td>Statistics - General (Also linked to Subjects)</td>
                                    <td>310 - 312</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                                                                                               
                                <tr class="Tt esc-guide">
                                    <td>T</td>
                                    <td colspan="2">No items listed</td>
                                </tr>                               
                                <tr class="Uu esc-guide">
                                    <td>U</td>
                                    <td colspan="2">No items listed</td>
                                </tr>                               
                                <tr class="Vv esc-guide">
                                    <td>V</td>
                                    <td colspan="2">No items listed</td>
                                </tr>                               
                                <tr class="Ww esc-guide">
                                    <td>Women's Studies</td>
                                    <td>396</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                                                                                               
                                <tr class="Ww esc-guide">
                                    <td>WHO (World Health Organisation) Collection</td>
                                    <td>Bay M</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                                                                                               
                                <tr class="Xx esc-guide">
                                    <td>X</td>
                                    <td colspan="2">No items listed</td>
                                </tr>                               
                                <tr class="Yy esc-guide">
                                    <td>Y</td>
                                    <td colspan="2">No items listed</td>
                                </tr>                               
                                <tr class="Zz esc-guide">
                                    <td>Zoology</td>
                                    <td>590 - 599</td>
                                    <td><a href="#" title="Blue 2" rel="blue2" class="esc-floor-link">Blue 2</a></td>
                                </tr>                                                                           
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>

                <div id="esc-plans" class="esc-widget" style="display:none; margin-left:10px; margin-right:10px;">                                  
	                <div style="margin-bottom: 0.5em; text-align:center;">
	                    <span style="color: #660099; font-size: 1.3em;"><strong>Floor plans</strong></span>
	                </div>                                        
                    <div id="esc-floor-plans-widgets">
                        <div data-role="content" id="plans-page1" class="esc-floor-plans-menu">
                            <div class="ui-form-row esc-top-row">
                                <div class="ui-form-row">
                                    <div title="Blue Ground" rel="blueG" class="esc-icon esc-4col-left esc-logo esc-floor-link esc-floor-plan-icon esc-bgrd-blue">
                                        <div><h3>Blue Ground</h3></div>
                                    </div>
                                    <div title="Blue 1" rel="blue1" class="esc-icon esc-4col-middle esc-logo esc-floor-link esc-floor-plan-icon esc-bgrd-blue">
                                        <div><h3>Blue 1</h3></div>
                                    </div>                                  
                                    <div title="Blue 2" rel="blue2" class="esc-icon esc-4col-middle esc-logo esc-floor-link esc-floor-plan-icon esc-bgrd-blue">
                                        <div><h3>Blue 2</h3></div>
                                    </div>
                                    <div title="Blue 3" rel="blue3" class="esc-icon esc-4col-right esc-logo esc-floor-link esc-floor-plan-icon esc-bgrd-blue">
                                        <div><h3>Blue 3</h3></div>
                                    </div>                                          
                                </div>                              
                            </div>                  

                            <div class="ui-form-row esc-middle-row">
                                <div class="ui-form-row">
                                    <div title="Blue 4" rel="blue4" class="esc-icon esc-4col-left esc-logo esc-floor-link esc-floor-plan-icon esc-bgrd-blue">
                                        <div><h3>Blue 4</h3></div>
                                    </div>          
                                    <div title="Green 3" rel="green3" class="esc-icon esc-4col-middle esc-logo esc-floor-link esc-floor-plan-icon esc-bgrd-green">
                                        <div><h3>Green 3</h3></div>
                                    </div>
                                    <div title="Orange 4" rel="orange4" class="esc-icon esc-4col-middle esc-logo esc-floor-link esc-floor-link esc-floor-plan-icon esc-bgrd-orange">
                                        <div><h3>Orange 4</h3></div>
                                    </div>
                                    <div title="Orange 5" rel="orange5" class="esc-icon esc-4col-right esc-logo esc-floor-link esc-floor-link esc-floor-plan-icon esc-bgrd-orange">
                                        <div><h3>Orange 5</h3></div>
                                    </div>          
                                </div>                              
                            </div>                  

                            <div class="ui-form-row esc-middle-row">
                                <div class="ui-form-row">
                                    <div title="Purple Ground" rel="purpleG" class="esc-icon esc-4col-left esc-logo esc-floor-link esc-floor-plan-icon esc-bgrd-purple">
                                        <div><h3>Purple Ground</h3></div>
                                    </div>          
                                    <div title="Purple 2" rel="purple2" class="esc-icon esc-4col-middle esc-logo esc-floor-link esc-floor-plan-icon esc-bgrd-purple">
                                        <div><h3>Purple 2</h3></div>
                                    </div>
                                    <div title="Purple 4" rel="purple4" class="esc-icon esc-4col-middle esc-logo esc-floor-link esc-floor-plan-icon esc-bgrd-purple">
                                        <div><h3>Purple 4</h3></div>
                                    </div>
                                    <div title="Red 2" rel="red2" class="esc-icon esc-4col-right esc-logo esc-floor-link esc-floor-plan-icon esc-bgrd-red">
                                        <div><h3>Red 2</h3></div>
                                    </div>          
                                </div>                              
                            </div>                  

                            <div class="ui-form-row esc-middle-row">
                                <div class="ui-form-row">
                                    <div title="Red 3" rel="red3" class="esc-icon esc-4col-left esc-logo esc-floor-link esc-floor-plan-icon esc-bgrd-red">
                                        <div><h3>Red 3</h3></div>
                                    </div>          
                                    <div class="esc-icon esc-4col-middle">
                                    </div>
                                    <div class="esc-icon esc-4col-middle">
                                    </div>
                                    <div class="esc-icon esc-4col-right">
                                    </div>

                                </div>                              
                            </div>                  
                        </div>                      
                    </div>
                    
                    <div id="floor-plans-container">
                        <div class="esc-floor-plan" id="blueG" style="display:none;">
                            <img class="esc-swipe" src="../resources/images/Blue Ground.jpg" alt="Blue Ground" title="Blue Ground" />
                        </div>

                        <div class="esc-floor-plan" id="blue1" style="display:none;">
                            <img class="esc-swipe" src="../resources/images/Blue 1.jpg" alt="Blue 1" title="Blue 1" />
                        </div>

                        <div class="esc-floor-plan" id="blue2" style="display:none;">
                            <img class="esc-swipe" src="../resources/images/Blue 2.jpg" alt="Blue 2" title="Blue 2" />
                        </div>

                        <div class="esc-floor-plan" id="blue3" style="display:none;">
                            <img class="esc-swipe" src="../resources/images/Blue 3.jpg" alt="Blue 3" title="Blue 3" />
                        </div>

                        <div class="esc-floor-plan" id="blue4" style="display:none;">
                            <img class="esc-swipe" src="../resources/images/Blue 4.jpg" alt="Blue 4" title="Blue 4" />
                        </div>

                        <div class="esc-floor-plan" id="green3" style="display:none;">
                            <img class="esc-swipe" src="../resources/images/Green 3.jpg" alt="Green 3" title="Green 3" />
                        </div>

                        <div class="esc-floor-plan" id="orange4" style="display:none;">
                            <img class="esc-swipe" src="../resources/images/Orange 4.jpg" alt="Orange 4" title="Orange 4" />
                        </div>

                        <div class="esc-floor-plan" id="orange5" style="display:none;">
                            <img class="esc-swipe" src="../resources/images/Orange 5.jpg" alt="Orange 5" title="Orange 5" />
                        </div>

                        <div class="esc-floor-plan" id="purpleG" style="display:none;">
                            <img class="esc-swipe" src="../resources/images/Purple Ground.jpg" alt="Purple Ground" title="Purple Ground" />
                        </div>

                        <div class="esc-floor-plan" id="purple2" style="display:none;">
                            <img class="esc-swipe" src="../resources/images/Purple 2.jpg" alt="Purple 2" title="Purple 2" />
                        </div>

                        <div class="esc-floor-plan" id="purple4" style="display:none;">
                            <img class="esc-swipe" src="../resources/images/Purple 4.jpg" alt="Purple 4" title="Purple 4" />
                        </div>

                        <div class="esc-floor-plan" id="red2" style="display:none;"> <!--  CM: fixed stretch issues -->
                            <!-- <img class="esc-swipe scale141f" src="../resources/images/red2FloorPlan.jpg" /> -->
                            <img class="esc-swipe" src="../resources/images/Red 2.jpg" alt="Red 2" title="Red 2" />
                        </div>
                        
                        <div class="esc-floor-plan" id="red3" style="display:none;">
                            <!-- <img class="esc-swipe scale141f" src="../resources/images/red3FloorPlan.jpg"/> -->
                            <img class="esc-swipe" src="../resources/images/Red 3.jpg" alt="Red 3" title="Red 3" />
                        </div>

                        <div class="esc-floor-plan mobile-device" id="placeholder" style="display:none;">
                             Oops! Sorry. We do not currently have a floor plan for this location.
                        </div>
                        
                    </div>
                    
                </div>

                <div id="esc-pc-clusters" class="esc-widget" style="display:none;">                                    
	                <div style="margin-bottom: 0.5em; text-align:center;">
	                    <!-- <span style="color: #660099; font-size: 1.3em;"><strong>PC availability</strong></span> -->
	                    <span style="color: #660099; font-size: 1.3em;"><strong></strong></span>
	                </div>
                <!-- <div id="esc-pc-clusters" class="esc-widget" style="display:none; margin-left:10px; margin-right:10px;"> -->                                    
                
                    <!-- Actual chart containers - start -->
                    <div style="overflow:auto; max-height:calc(100vh - 220px);">
                        <div id="esc-pc-all-cluster" class="esc-pc-all-cluster esc-widget esc-pc-cluster" style="display:none; min-height:500px;">
                            <div id="esc-pc-all" style="overflow:scroll;">
                            </div>
                            <div id="esc-updated-all-div" style="text-align: right;">
                                <span style="margin-right:5px;">Last updated:</span><span id="esc-last-updated-all-input"></span>
                            </div>                                                                                                                                  
                        </div>
                                            
                    </div>
                                        
                    <!-- Actual chart containers - end -->
                    
                </div>

                <div id="alert-dialog" class="esc-alert" style="display: none;">
                    <div id="message-div" class="mobile-device">
                    </div>
                    <div id="feedback-div" style="text-align:left; border-top:1px double #CCCCCC; margin-top:20px; padding-top:10px; display:table; width:100%">
                    	<div style="display:table-row">
                    		<div style="display:table-cell; width:60px;">
		                        <img id="esc-contact-img" class="esc-nav esc-logo" rel="esc-contact" src="../resources/images/Contact6.png" alt="Feedback" title="Feedback" />
                    		</div>
                    		<div style="display:table-cell;">
		                        <a href="javascript:this.preventDefault" class="esc-nav esc-logo" rel="esc-contact">Tell us what you think</a> about this app.  All comments, criticisms, recommendations welcome.                    		
                    		</div>
                    	</div>
                    </div>                    
                </div>                           
                
            </div>
            
            
            
	       <div id="esc-opening-times" class="esc-widget" style="display:none;">
	           <div style="margin-bottom: 0.5em; text-align:center;">
	               <span style="color: #660099; font-size: 1.3em;"><strong>Opening times</strong></span>
	           </div>
				<c:catch var="exception">
					<c:import var = "openXML" url = "http://systems.library.manchester.ac.uk:8080/blackboard_webservices/places?tag=10"/>
					<x:parse xml="${openXML}" var="result"/>
					<c:set var="zebra" value=""/>
	                  <!-- <div style="padding-left:25%; padding-right:25%; overflow:auto; max-height:calc(100vh - 240px);"> -->
	                  <div style="overflow:auto; max-height:calc(100vh - 240px); padding-left:10%; padding-right:10%;">
	                      <div id="esc-opening-times-container" style="max-height:inherit; margin-left:10px; margin-right:10px;">		                      	
							<x:forEach select="$result/data/PLPlace" var="item" varStatus="counter">
								<%-- <x:set var="code" select="$item/locationCode" /> --%>
								<%-- <c:set var="name" value="${fn:replace(${code}, ' - info', '')"/> --%>
								<div class="table opening-time" data-id="<x:out select='$item/locationCode'/>${counter.index}">
									<div class="table-row">
										<%-- <div class="table-cell opening-time" data-id="<x:out select='$item/locationCode'/>">+</div> --%>
										<!-- <div class="table-cell symbol" style="width:1%;">+</div> -->
										<x:choose>
											<x:when select="$item/name = 'Ahmed Iqbal Ullah Race Relations Resource Centre'">
												<div class="table-cell opening-time-head">
													<div style="display:inline-block;">
														<h3 class="largeTitle"><x:out select="'Ahmed Iqbal Ullah Race Relations Resource Centre'"/></h3>
													</div>
													<div style="display:inline-block">
														<!-- <a href="javascript:console.log('handle map here...');">MAP Image to go here.</a> -->
														<img class="map-link" src="../resources/images/MapMarker.png" data-id="104" title="Link to map" />
													</div>												
												</div>
											</x:when>
											<x:when select="$item/name = 'Alan Gilbert Learning Commons - info'">
												<div class="table-cell opening-time-head">
													<div style="display:inline-block;">
														<h3 class="largeTitle"><x:out select="'Alan Gilbert Learning Commons'"/></h3>													
													</div>													
													<div style="display:inline-block">
														<!-- <a href="javascript:console.log('handle map here...');">MAP Image to go here.</a> -->
														<img class="map-link" src="../resources/images/MapMarker.png" data-id="386" title="Link to map" />
													</div>																																																		
												</div>												
											</x:when>
											<x:when select="$item/name = 'Art and Archaeology Site Library '">
												<div class="table-cell opening-time-head">
													<div style="display:inline-block;">
														<h3 class="largeTitle"><x:out select="'Art and Archaeology Site Library'"/></h3>
													</div>
													<div style="display:inline-block">
														<img class="map-link" src="../resources/images/MapMarker.png" alt="Link to map" title="Link to map" data-id="413" />
													</div>																									
												</div>												
											</x:when>												
											
											<x:when select="$item/name = 'Braddick Library '">
												<div class="table-cell opening-time-head">
													<div style="display:inline-block;">
														<h3 class="largeTitle"><x:out select="'Braddick Library'"/></h3> 														
													</div>
	  												<div style="display:inline-block">
														<img class="map-link" src="../resources/images/MapMarker.png" data-id="117" title="Link to map" />
													</div>																																						
												</div>												
											</x:when>
											<x:when select="$item/name = 'John Rylands Library - info'">
												<div class="table-cell opening-time-head">
													<div style="display:inline-block;">
														<h3 class="largeTitle"><x:out select="'John Rylands Library'"/></h3>
													</div>	
	 												<div style="display:inline-block">
														<img class="map-link" src="../resources/images/MapMarker.png" data-id="414" title="Link to map" />
													</div>																																					
												</div>												
											</x:when>
											<x:when select="$item/name = 'Joule Library'">											
												<div class="table-cell opening-time-head">
													<div style="display:inline-block;">
														<h3 class="largeTitle"><x:out select="'Joule Library'"/></h3>
													</div>
	  												<div style="display:inline-block">
														<img class="map-link" src="../resources/images/MapMarker.png" data-id="207" title="Link to map" />
													</div>																																						
												</div>																							
											</x:when>
											<x:when select="$item/name = 'Kantorowich Library'">											
												<div class="table-cell opening-time-head">
													<div style="display:inline-block;">
														<h3 class="largeTitle"><x:out select="'Kantorowich Library'"/></h3>
													</div>
 	  												<div style="display:inline-block">
														<img class="map-link" src="../resources/images/MapMarker.png" data-id="209" title="Link to map" />
													</div>												
																										
												</div>																							
											</x:when>
											<x:when select="$item/name = 'Lenagan Library '">
												<div class="table-cell opening-time-head">
													<div style="display:inline-block;">
														<h3 class="largeTitle"><x:out select="'Lenagan Library'"/></h3>
													</div>
 	  												<div style="display:inline-block">
														<img class="map-link" src="../resources/images/MapMarker.png" data-id="218" title="Link to map" />
													</div>																																						
												</div>																							
											</x:when>
											<x:when select="$item/name = 'Library Finance Zone @ Dover Street'">
												<div class="table-cell opening-time-head">
													<div style="display:inline-block;">
														<h3 class="largeTitle"><x:out select="'Library Finance Zone @ Dover Street'"/></h3>
													</div>
 	  												<div style="display:inline-block">
														<img class="map-link" src="../resources/images/MapMarker.png" data-id="66" title="Link to map" />
													</div>																																						
												</div>																							
											</x:when>
											<x:when select="$item/name = 'Main Library - info'">
												<div class="table-cell opening-time-head">
													<div style="display:inline-block;">
														<h3 class="largeTitle"><x:out select="'Main Library'"/></h3>
													</div>
	  												<div style="display:inline-block">
														<img class="map-link" src="../resources/images/MapMarker.png" data-id="53" title="Link to map" />
													</div>																																						
												</div>																																															
											</x:when>
											<x:when select="$item/name = 'Precinct Library'">
												<div class="table-cell opening-time-head">
													<div style="display:inline-block;">
														<h3 class="largeTitle"><x:out select="'Precinct Library'"/></h3>
													</div>
 	  												<div style="display:inline-block">
														<img class="map-link" src="../resources/images/MapMarker.png" data-id="261" title="Link to map" />
													</div>																																						
												</div>
											</x:when>
											<x:when select="$item/name = 'Stopford Library'">
												<div class="table-cell opening-time-head">
													<div style="display:inline-block;">
														<h3 class="largeTitle"><x:out select="'Stopford Library'"/></h3>
													</div>
 	  												<div style="display:inline-block">
														<img class="map-link" src="../resources/images/MapMarker.png" data-id="451" title="Link to map" />
													</div>																																						
												</div>																								
											</x:when>
											<x:when select="$item/name = 'The Tabley House Collection - Country House Library '">
												<div class="table-cell opening-time-head">
													<div style="display:inline-block;">
														<h3 class="largeTitle"><x:out select="'The Tabley House Collection - Country House Library'"/></h3>
													</div>
 	  												<div style="display:inline-block">
														<img class="map-link" src="../resources/images/MapMarker.png" data-id="503" title="Link to map" />
													</div>																																						
												</div>																								
											</x:when>
											<x:otherwise>
												<div class="table-cell opening-time-head">
													<div style="display:inline-block;">
														<h3 class="largeTitle"><x:out select="$item/name"/></h3>
													</div>
 	  												<div style="display:inline-block">
														<img class="map-link" src="../resources/images/MapMarker.png" data-id="" title="Link to map" />
													</div>																																						
												</div>																								
											</x:otherwise>
										</x:choose>																				
									</div>
									<div class="table-row opening-time-detail" style="display:none;">
										<div class="table" style="width:100%;">
										<div class="table-row">
										<div class="table-cell" style="width:50%;">
											<x:choose>
												<x:when select="$item/name = 'Ahmed Iqbal Ullah Race Relations Resource Centre'">
													<img class="detail-img" src="../resources/images/AIURRRC2.jpg" alt="AIURRRC" title="AIURRRC" data-id="104" />
												</x:when>
												<x:when select="$item/name = 'Alan Gilbert Learning Commons - info'">
													<img class="detail-img" src="../resources/images/AGLC.jpg" alt="AGLC" title="AGLC" data-id="386" />
												</x:when>
												<x:when select="$item/name = 'Art and Archaeology Site Library '">
													<img class="detail-img" src="../resources/images/art-and-archaeology.jpg" alt="Art and Archaeology" title="Art and Archaeology" data-id="413" />
												</x:when>
												<x:when select="$item/name = 'Braddick Library '">
													<img class="detail-img" src="../resources/images/Braddick.jpg" alt="Braddick Library" title="Braddick Library" data-id="117" />
												</x:when>
												<x:when select="$item/name = 'John Rylands Library - info'">
													<img class="detail-img" src="../resources/images/TJRL.jpg" alt="John Rylands Library" title="John Rylands Library" data-id="414" />
												</x:when>
												<x:when select="$item/name = 'Joule Library'">											
													<img class="detail-img" src="../resources/images/Joule.jpg" alt="Joule Library" title="Joule Library" data-id="207" />
												</x:when>
												<x:when select="$item/name = 'Kantorowich Library'">											
													<img class="detail-img" src="../resources/images/Kant.jpg" alt="Kantorowich Library" title="Kantorowich Library" data-id="209" />
												</x:when>
												<x:when select="$item/name = 'Lenagan Library '">
													<img class="detail-img" src="../resources/images/Lenagan.jpg" alt="Lenagan Library" title="Lenagan Library" data-id="218" />
												</x:when>
												<x:when select="$item/name = 'Library Finance Zone @ Dover Street'">
													<img class="detail-img" src="../resources/images/Dover-street.jpg" alt="Library Finance Zone @ Dover Street" title="Library Finance Zone @ Dover Street" data-id="66" />
												</x:when>
												<x:when select="$item/name = 'Main Library - info'">
													<img class="detail-img" src="../resources/images/Main-Library.jpg" alt="Main Library" title="Main Library" data-id="53" />
												</x:when>
												<x:when select="$item/name = 'Precinct Library'">
													<img class="detail-img" src="../resources/images/Precinct-Steps.jpg" alt="Precinct Library" title="Precinct Library" data-id="261" />
												</x:when>
												<x:when select="$item/name = 'Stopford Library'">
													<img class="detail-img" src="../resources/images/Stopford.jpg" alt="Stopford Library" title="Stopford Library" data-id="415" />
												</x:when>
												<x:when select="$item/name = 'The Tabley House Collection - Country House Library '">
													<img class="detail-img" src="../resources/images/Tabley2.jpg" alt="Country House Library" title="Country House Library" data-id="503" />
												</x:when>
												<x:otherwise>
													<img class="detail-img" src="../resources/images/uni.jpg" alt="<x:out select="$item/name"/>" title="<x:out select="$item/name"/>"  data-id="" />
												</x:otherwise>
											</x:choose>
										</div>
										
										<div class="table-cell" style="width:50%; padding-left:10px;">
											<div id="<x:out select='$item/locationCode'/>${counter.index}">
												<div class="table" style="width:100%; font-size:1.5em;">
													<x:choose>
														<x:when select="$item/times/time/day = 'Sunday'">
															<div class="table-row esc-zebra">
																<div class="table-cell mobile-device" style="width:40%; padding-left:5px;">Sunday</div>
																<div class="table-cell mobile-device"><x:out select="$item/times/time[day = 'Sunday']/hours/hour/start"/> - <x:out select="$item/times/time[day = 'Sunday']/hours/hour/end"/></div>
															</div>									
														</x:when>
														<x:otherwise>
															<div class="table-row esc-zebra">
																<div class="table-cell mobile-device" style="width:40%; padding-left:5px;">Sunday</div>
																<div class="table-cell mobile-device">Closed</div>
															</div>									
														</x:otherwise>
													</x:choose>
													<x:choose>
														<x:when select="$item/times/time/day = 'Monday'">
															<div class="table-row">
																<div class="table-cell mobile-device" style="padding-left:5px;">Monday</div>
																<div class="table-cell mobile-device"><x:out select="$item/times/time[day = 'Monday']/hours/hour/start"/> - <x:out select="$item/times/time[day = 'Monday']/hours/hour/end"/></div>
															</div>									
														</x:when>
														<x:otherwise>
															<div class="table-row">
																<div class="table-cell mobile-device" style="padding-left:5px;">Monday</div>
																<div class="table-cell mobile-device">Closed</div>
															</div>									
														</x:otherwise>
													</x:choose>
													<x:choose>
														<x:when select="$item/times/time/day = 'Tuesday'">
															<div class="table-row esc-zebra">
																<div class="table-cell mobile-device" style="padding-left:5px;">Tuesday</div>
																<div class="table-cell mobile-device"><x:out select="$item/times/time[day = 'Tuesday']/hours/hour/start"/> - <x:out select="$item/times/time[day = 'Tuesday']/hours/hour/end"/></div>
															</div>									
														</x:when>
														<x:otherwise>
															<div class="table-row esc-zebra">
																<div class="table-cell mobile-device" style="padding-left:5px;">Tuesday</div>
																<div class="table-cell mobile-device">Closed</div>
															</div>									
														</x:otherwise>
													</x:choose>
													<x:choose>
														<x:when select="$item/times/time/day = 'Wednesday'">
															<div class="table-row">
																<div class="table-cell mobile-device" style="padding-left:5px;">Wednesday</div>
																<div class="table-cell mobile-device"><x:out select="$item/times/time[day = 'Wednesday']/hours/hour/start"/> - <x:out select="$item/times/time[day = 'Wednesday']/hours/hour/end"/></div>
															</div>									
														</x:when>
														<x:otherwise>
															<div class="table-row">
																<div class="table-cell mobile-device" style="padding-left:5px;">Wednesday</div>
																<div class="table-cell mobile-device">Closed</div>
															</div>									
														</x:otherwise>
													</x:choose>
													<x:choose>
														<x:when select="$item/times/time/day = 'Thursday'">
															<div class="table-row esc-zebra">
																<div class="table-cell mobile-device" style="padding-left:5px;">Thursday</div>
																<div class="table-cell mobile-device"><x:out select="$item/times/time[day = 'Thursday']/hours/hour/start"/> - <x:out select="$item/times/time[day = 'Thursday']/hours/hour/end"/></div>
															</div>									
														</x:when>
														<x:otherwise>
															<div class="table-row esc-zebra">
																<div class="table-cell mobile-device" style="padding-left:5px;">Thursday</div>
																<div class="table-cell mobile-device">Closed</div>
															</div>									
														</x:otherwise>
													</x:choose>
													<x:choose>
														<x:when select="$item/times/time/day = 'Friday'">
															<div class="table-row">
																<div class="table-cell mobile-device" style="padding-left:5px;">Friday</div>
																<div class="table-cell mobile-device"><x:out select="$item/times/time[day = 'Friday']/hours/hour/start"/> - <x:out select="$item/times/time[day = 'Friday']/hours/hour/end"/></div>
															</div>									
														</x:when>
														<x:otherwise>
															<div class="table-row">
																<div class="table-cell mobile-device" style="padding-left:5px;">Friday</div>
																<div class="table-cell mobile-device">Closed</div>
															</div>									
														</x:otherwise>
													</x:choose>
													<x:choose>
														<x:when select="$item/times/time/day = 'Saturday'">
															<div class="table-row esc-zebra">
																<div class="table-cell mobile-device" style="padding-left:5px;">Saturday</div>
																<div class="table-cell mobile-device"><x:out select="$item/times/time[day = 'Saturday']/hours/hour/start"/> - <x:out select="$item/times/time[day = 'Saturday']/hours/hour/end"/></div>
															</div>									
														</x:when>
														<x:otherwise>
															<div class="table-row esc-zebra">
																<div class="table-cell mobile-device" style="padding-left:5px;">Saturday</div>
																<div class="table-cell mobile-device">Closed</div>
															</div>									
														</x:otherwise>
													</x:choose>
												</div>
											</div>										
										</div>
										</div>																		
									</div>
								</div>
								</div>
							</x:forEach>	                        		                        	
	                      </div>		                      							
					</div>
				</c:catch>
	        <!-- BA: Show static default if dynamic xml cannot be retrieved - start  -->               					
			<c:if test="${exception != null}">	
<%-- 				
				<div>
					Exception = ${exception }
				</div>
 --%>					                
	            <div style="margin-left:10px; margin-right:10px; max-height:calc(100vh - 240px); display:inherit;">
	                <div id="esc-opening-times-container" style="overflow:auto; max-height:inherit;">
	                    <table id="esc-opening-times-table" title="Opening Times"><thead><tr><th>Location</th><th>Sunday</th><th>Monday</th><th>Tuesday</th><th>Wednesday</th><th>Thursday</th><th>Friday</th><th>Saturday</th></tr></thead><tbody><tr><td>Joule Library</td><td>Closed</td><td>09:30 - 16:30</td><td>09:30 - 16:30</td><td>09:30 - 16:30</td><td>09:30 - 16:30</td><td>09:30 - 16:30</td><td>Closed</td></tr><tr class="esc-zebra"><td>Alan Gilbert Learning Commons</td><td>00:00 - 23:59</td><td>00:00 - 23:59</td><td>00:00 - 23:59</td><td>00:00 - 23:59</td><td>00:00 - 23:59</td><td>00:00 - 23:59</td><td>00:00 - 23:59</td></tr><tr><td>Mansfield Cooper 2.1</td><td>Closed</td><td>09:30 - 19:00</td><td>09:30 - 19:00</td><td>09:30 - 19:00</td><td>09:30 - 19:00</td><td>09:30 - 19:00</td><td>Closed</td></tr><tr class="esc-zebra"><td>Schuster Building M (School of Physics)</td><td>Closed</td><td>09:30 - 13:00</td><td>09:30 - 13:00</td><td>09:30 - 13:00</td><td>09:30 - 13:00</td><td>09:30 - 13:00</td><td>Closed</td></tr><tr><td>John Rylands Library</td><td>12:00 - 17:00</td><td>12:00 - 17:00</td><td>10:00 - 17:00</td><td>10:00 - 17:00</td><td>10:00 - 17:00</td><td>10:00 - 17:00</td><td>10:00 - 17:00</td></tr><tr class="esc-zebra"><td>Martin Harris F31/32</td><td>Closed</td><td>09:30 - 17:00</td><td>09:30 - 17:00</td><td>09:30 - 17:00</td><td>09:30 - 17:00</td><td>09:30 - 17:00</td><td>Closed</td></tr><tr><td>Library Finance Zone@Dover Street</td><td>Closed</td><td>08:00 - 16:00</td><td>08:00 - 16:00</td><td>08:00 - 16:00</td><td>08:00 - 16:00</td><td>08:00 - 16:00</td><td>Closed</td></tr><tr class="esc-zebra"><td>Main Library</td><td>09:00 - 23:59</td><td>08:00 - 23:59</td><td>08:00 - 23:59</td><td>08:00 - 23:59</td><td>08:00 - 23:59</td><td>08:00 - 23:59</td><td>09:00 - 23:59</td></tr><tr><td>Precinct Library</td><td>13:00 - 18:00</td><td>09:00 - 20:00</td><td>09:00 - 20:00</td><td>09:00 - 20:00</td><td>09:00 - 20:00</td><td>09:00 - 20:00</td><td>10:00 - 18:00</td></tr><tr class="esc-zebra"><td>Stopford PC Cluster</td><td>Closed</td><td>09:00 - 22:00</td><td>09:00 - 22:00</td><td>09:00 - 22:00</td><td>09:00 - 22:00</td><td>09:00 - 17:00</td><td>Closed</td></tr></tbody></table>                           
	                </div>
	            </div>	                    										
			</c:if>								
	        <!-- BA: Show static default if dynamic xml cannot be retrieved - end  -->               				
	   	</div>
	            	                 	                         
            <!-- List of widget contents - end -->          
                                                    

        <div id="loading-dialog" style="display: none;">
            <div>
                <img alt="loading" src="../resources/css/images/ajaxloading.gif" width="64" height="64" class="esc-loading"/>
            </div>
        </div>



		<!-- Place this script as near to the end of your BODY as possible. -->
		<script type="text/javascript">// <![CDATA[
		(function() {
		    var x = document.createElement("script"); x.type = "text/javascript"; x.async = true;
		    x.src = (document.location.protocol === "https:" ? "https://" : "http://") + "eu.libraryh3lp.com/js/libraryh3lp.js?725"
		    var y = document.getElementsByTagName("script")[0]; y.parentNode.insertBefore(x, y);
		  })();
		// ]]></script>




    
        <footer id="main-footer">
            <div class="esc-home-div" style="display:inline">
                
                <!-- Floor plans widgets - start -->
                <div id="esc-plans-widgets" class="esc-nav-widgets esc-floor-plan-widgets" style="display:none;">
                    <div style="display:inline;">
                        <div style="display:inline; margin-top:20px; position:relative; top:5px;">
                            <!-- <span class="esc-back" style="display:none;"><img alt="Back" src="../resources/images/Left-20.png" title="Back" /></span> -->
                            <span class="esc-back" style="display:none;"><img alt="Back" src="../resources/images/back3.png" title="Back" style="max-height:1em; width:auto;" /></span>
                        </div>
                        <div style="display:inline;">
                            <span class="esc-floor-plan-widget esc-floor-link esc-blue" rel="blueG" title="Blue Ground" alt="Blue Ground">Blue G</span>
                             | 
                            <span class="esc-floor-plan-widget esc-floor-link esc-blue" rel="blue1" title="Blue 1" alt="Blue 2">Blue 1</span>
                             | 
                            <span class="esc-floor-plan-widget esc-floor-link esc-blue" rel="blue2" title="Blue 2" alt="Blue 2">Blue 2</span>
                             | 
                            <span class="esc-floor-plan-widget esc-floor-link esc-blue" rel="blue3" title="Blue 3" alt="Blue 3">Blue 3</span>
                             | 
                            <span class="esc-floor-plan-widget esc-floor-link esc-blue" rel="blue4" title="Blue 4" alt="Blue 4">Blue 4</span>
                             | 
                            <span class="esc-floor-plan-widget esc-floor-link esc-green" rel="green3" title="Green 3" alt="Green 3">Green 3</span>
                             | 
                            <span class="esc-floor-plan-widget esc-floor-link esc-orange" rel="orange4" title="Orange 4" alt="Orange 4">Orange 4</span>
                             | 
                            <span class="esc-floor-plan-widget esc-floor-link esc-orange" rel="orange5" title="Orange 5" alt="Orange 5">Orange 5</span>
                             |
                            <span class="esc-floor-plan-widget esc-floor-link esc-purple" rel="purpleG" title="Purple Ground" alt="Purple Ground">Purple G</span>
                             | 
                            <span class="esc-floor-plan-widget esc-floor-link esc-purple" rel="purple2" title="Purple 2" alt="Purple 2">Purple 2</span>
                             | 
                            <span class="esc-floor-plan-widget esc-floor-link esc-purple" rel="purple4" title="Purple 4" alt="Purple 4">Purple 4</span>
                             | 
                            <span class="esc-floor-plan-widget esc-floor-link esc-red" rel="red2" title="Red 2" alt="Red 2">Red 2</span>
                             | 
                            <span class="esc-floor-plan-widget esc-floor-link esc-red" rel="red3" title="Red 3" alt="Red 3">Red 3</span>
                        </div>
                    </div>                    
                </div>              
                <!-- Floor plans widgets - end -->
                                
            </div>      
        </footer>
                
    </body>
</html>

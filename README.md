# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

The Library Information Point (LIP) project (formerly kiosk project) is a standalone single page application built with JSP, HTML5, CSS and jQuery. All dependencies are included in the resources folder.
It has been redeveloped with jsp technology to run on a Tomcat 7 server.

### How do I get set up? ###


#Add a comment to this line
It is monitored using Google Analytics with the credentials:
Username: DLADT UoM
Email: DLADT.UoM@gmail.com
Pwd: this is the same password we used for PuTTY to access the old servers.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

This project is managed by Berrisford Edwards. Contact Berrisford.Edwards@manchester.ac.uk for any info.